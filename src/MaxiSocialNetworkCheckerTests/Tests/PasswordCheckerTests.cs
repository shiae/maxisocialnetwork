using MaxiSocialNetworkChecker;
using MaxiSocialNetworkChecker.Exceptions;

namespace MaxiSocialNetworkCheckerTests
{
    [TestClass]
    public class PasswordCheckerTests
    {
        private PasswordChecker _passwordChecker;

        [TestInitialize]
        public void Setup()
        {
            _passwordChecker = new PasswordChecker();
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PasswordChecker.MINIMUL_LENGTH_ERROR)]
        public void PasswordChecker_Null_ThrowNotValidPasswordException()
        {
            string? password = null;
            _passwordChecker.Check(password);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PasswordChecker.MINIMUL_LENGTH_ERROR)]
        public void PasswordChecker_StringWith4Symbols_ThrowNotValidPasswordException()
        {
            string? password = "1234";
            _passwordChecker.Check(password);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PasswordChecker.FORMAT_NOT_MATCH)]
        public void PasswordChecker_StringWithErrorFormat_ThrowNotValidPasswordException()
        {
            string? password = "1234678";
            _passwordChecker.Check(password);
        }

        [TestMethod]

        public void PasswordChecker_StringWithValidFormat_VoidReturned()
        {
            string? password = "1234678Aa$";
            _passwordChecker.Check(password);
        }
    }
}