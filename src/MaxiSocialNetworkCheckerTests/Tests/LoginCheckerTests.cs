﻿using MaxiSocialNetworkChecker.Exceptions;
using MaxiSocialNetworkChecker;

namespace MaxiSocialNetworkCheckerTests
{
    [TestClass]
    public class LoginCheckerTests
    {
        [TestMethod]
        public void Check_ValidLogin_DoesNotThrowException()
        {
            var loginChecker = new LoginChecker();
            string validLogin = "JohnDoe123";

            loginChecker.Check(validLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_CAN_NOT_BE_EMPTY)]
        public void Check_NullLogin_ThrowsExceptionWithLoginCanNotBeEmptyMessage()
        {
            var loginChecker = new LoginChecker();
            string nullLogin = null;

            loginChecker.Check(nullLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_CAN_NOT_BE_EMPTY)]
        public void Check_EmptyLogin_ThrowsExceptionWithLoginCanNotBeEmptyMessage()
        {
            var loginChecker = new LoginChecker();
            string emptyLogin = string.Empty;

            loginChecker.Check(emptyLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_CAN_CONTAINS_LETTERS_AND_DIGITS)]
        public void Check_LoginWithSpecialCharacters_ThrowsExceptionWithLoginCanContainsLettersAndDigitsMessage()
        {
            var loginChecker = new LoginChecker();
            string invalidLogin = "John@Doe!";

            loginChecker.Check(invalidLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_CAN_CONTAINS_LETTERS_AND_DIGITS)]
        public void Check_LoginWithRussianLetters_ThrowsExceptionWithLoginCanContainsLettersAndDigitsMessage()
        {
            var loginChecker = new LoginChecker();
            string invalidLogin = "Иван123";

            loginChecker.Check(invalidLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_MUST_CONTAINS_LETTERS_AND_DIGITS)]
        public void Check_LoginWithOnlyLetters_ThrowsExceptionWithLoginMustContainsLettersAndDigitsMessage()
        {
            var loginChecker = new LoginChecker();
            string invalidLogin = "JohnDoe";

            loginChecker.Check(invalidLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_MUST_CONTAINS_LETTERS_AND_DIGITS)]
        public void Check_LoginWithOnlyDigits_ThrowsExceptionWithLoginMustContainsLettersAndDigitsMessage()
        {
            var loginChecker = new LoginChecker();
            string invalidLogin = "123456";

            loginChecker.Check(invalidLogin);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), LoginChecker.LOGIN_MUST_CONTAINS_LETTERS_AND_DIGITS)]
        public void Check_LoginWithSpaces_ThrowsExceptionWithLoginMustContainsLettersAndDigitsMessage()
        {
            var loginChecker = new LoginChecker();
            string invalidLogin = "John Doe 123";

            loginChecker.Check(invalidLogin);
        }

        [TestMethod]
        public void Check_LoginWithMaxLength_DoesNotThrowException()
        {
            var loginChecker = new LoginChecker();
            string maxLogin = new string('A', 20) + new string('1', 20);

            loginChecker.Check(maxLogin);
        }
    }
}
