﻿using MaxiSocialNetworkChecker.Exceptions;
using MaxiSocialNetworkChecker;

namespace MaxiSocialNetworkCheckerTests
{
    [TestClass]
    public class PhoneCheckerTests
    {
        private PhoneChecker _phoneChecker;

        [TestInitialize]
        public void Initialize()
        {
            _phoneChecker = new PhoneChecker();
        }

        [TestMethod]
        public void CheckPhoneNumber_ValidPhoneNumber_ShouldNotThrowException()
        {
            string phoneNumber = "+79123456789";
            _phoneChecker.Check(phoneNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PhoneChecker.NOT_CORRECT_PHONE_ERROR_MSG)]
        public void CheckPhoneNumber_InvalidPhoneNumber_ShouldThrowException()
        {
            string phoneNumber = "12345";
            _phoneChecker.Check(phoneNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PhoneChecker.NOT_CORRECT_PHONE_ERROR_MSG)]
        public void CheckPhoneNumber_InvalidPhoneNumberWithLetters_ShouldThrowException()
        {
            string phoneNumber = "12345grfegregre";
            _phoneChecker.Check(phoneNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PhoneChecker.NOT_CORRECT_PHONE_ERROR_MSG)]
        public void CheckPhoneNumber_InvalidPhoneNumberWithSpacesAndLetters_ShouldThrowException()
        {
            string phoneNumber = "12345grf   egregre";
            _phoneChecker.Check(phoneNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), PhoneChecker.NOT_CORRECT_PHONE_ERROR_MSG)]
        public void CheckPhoneNumber_EmptyPhoneNumber_ShouldThrowException()
        {
            string phoneNumber = "";
            _phoneChecker.Check(phoneNumber);
        }
    }
}
