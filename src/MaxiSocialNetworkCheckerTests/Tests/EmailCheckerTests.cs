﻿using MaxiSocialNetworkChecker;
using MaxiSocialNetworkChecker.Exceptions;

namespace MaxiSocialNetworkCheckerTests
{
    [TestClass]
    public class EmailCheckerTests
    {
        private EmailChecker _emailChecker;

        [TestInitialize]
        public void Setup()
        {
            _emailChecker = new EmailChecker();
        }

        [TestMethod]
        public void TestValidEmail()
        {
            string email = "test@example.com";
            _emailChecker.Check(email);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), EmailChecker.NOT_CORRECT_EMAIL_ERROR_MSG)]
        public void TestInvalidEmailNoAtSymbol()
        {
            string email = "test.example.com";
            _emailChecker.Check(email);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), EmailChecker.NOT_CORRECT_EMAIL_ERROR_MSG)]
        public void TestInvalidEmailNoDomain()
        {
            string email = "test@";
            _emailChecker.Check(email);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), EmailChecker.NOT_CORRECT_EMAIL_ERROR_MSG)]
        public void TestInvalidEmailNoTopLevelDomain()
        {
            string email = "test@example.";
            _emailChecker.Check(email);
        }

        [TestMethod]
        [ExpectedException(typeof(BaseCheckerException), EmailChecker.NOT_CORRECT_EMAIL_ERROR_MSG)]
        public void TestInvalidEmailWithSpace()
        {
            string email = "test @example.com";
            _emailChecker.Check(email);
        }
    }
}
