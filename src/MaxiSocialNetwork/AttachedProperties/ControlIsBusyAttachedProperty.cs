﻿using System.Windows.Controls;
using System.Windows;

namespace MaxiSocialNetwork
{
    public class ControlIsBusyAttachedProperty
    {
        #region Публичные статичные свойства

        /// <summary>
        /// Определяет свойство, которое проверяет, занят ли элемент пользовательского интерфейса или нет
        /// </summary>
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.RegisterAttached("IsBusy", typeof(bool), typeof(ControlIsBusyAttachedProperty), new PropertyMetadata(false));

        #endregion

        #region Публичные статичные методы

        /// <summary>
        /// Возвращает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool GetIsBusy(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsBusyProperty);
        }

        /// <summary>
        /// Устанавливает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetIsBusy(DependencyObject obj, bool value)
        {
            obj.SetValue(IsBusyProperty, value);
        }

        #endregion
    }
}
