﻿using System.Windows;
using System.Windows.Controls;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Класс, определяющий самописные свойства для <see cref="TextBox"/>
    /// </summary>
    public class TextBoxAttachedProperty
    {
        #region Публичные статичные свойства

        /// <summary>
        /// Определяет свойство, которое устанавливает ошибку в элемент <see cref="TextBox"/>
        /// </summary>
        public static readonly DependencyProperty IsErrorProperty =
            DependencyProperty.RegisterAttached("IsError", typeof(bool), typeof(TextBoxAttachedProperty), new PropertyMetadata(null));

        #endregion

        #region Публичные статичные методы

        /// <summary>
        /// Возвращает ошибку
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetIsError(TextBox element)
        {
            return (bool)element.GetValue(IsErrorProperty);
        }

        /// <summary>
        /// Устанавливает ошибку
        /// </summary>
        /// <param name="element"></param>
        public static void SetIsError(TextBox element, bool value)
        {
            element.SetValue(IsErrorProperty, value);
        }

        #endregion
    }
}
