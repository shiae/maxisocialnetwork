﻿using System.Windows;
using System.Windows.Controls;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Класс, определяющий самописные свойства для <see cref="PasswordBox"/>
    /// </summary>
    public class PasswordBoxAttachedProperty
    {
        #region Публичные статичные свойства

        /// <summary>
        /// Определеяет свойство, которое устанавливает слежку за <see cref="PasswordBox"/>
        /// </summary>
        public static readonly DependencyProperty MonitorPasswordProperty =
            DependencyProperty.RegisterAttached("MonitorPassword", typeof(bool), typeof(PasswordBoxAttachedProperty), new PropertyMetadata(false, OnMonitorPasswordChanged));

        /// <summary>
        /// Определяет свойство, которое проверяет, есть ли в <see cref="PasswordBox"/> текст или нет
        /// </summary>
        public static readonly DependencyProperty HasTextProperty =
            DependencyProperty.RegisterAttached("HasText", typeof(bool), typeof(PasswordBoxAttachedProperty), new PropertyMetadata(false));

        /// <summary>
        /// Определяет свойство, которое устанавливает ошибку в элемент <see cref="PasswordBox"/>
        /// </summary>
        public static readonly DependencyProperty IsErrorProperty =
            DependencyProperty.RegisterAttached("IsError", typeof(bool), typeof(PasswordBoxAttachedProperty), new PropertyMetadata(null));

        #endregion

        #region Публичные статичные методы

        /// <summary>
        /// Возвращает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetHasText(PasswordBox element)
        {
            return (bool)element.GetValue(HasTextProperty);
        }

        /// <summary>
        /// Устанавливает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="element"></param>
        public static void SetHasText(PasswordBox element)
        {
            element.SetValue(HasTextProperty, element.SecurePassword.Length > 0);
        }

        /// <summary>
        /// Возвращает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool GetMonitorPassword(DependencyObject obj)
        {
            return (bool)obj.GetValue(MonitorPasswordProperty);
        }

        /// <summary>
        /// Устанавливает признак заполненности <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="value"></param>
        public static void SetMonitorPassword(DependencyObject obj, bool value)
        {
            obj.SetValue(MonitorPasswordProperty, value);
        }

        /// <summary>
        /// Возвращает ошибку
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool GetIsError(PasswordBox element)
        {
            return (bool)element.GetValue(IsErrorProperty);
        }

        /// <summary>
        /// Устанавливает ошибку
        /// </summary>
        /// <param name="element"></param>
        public static void SetIsError(PasswordBox element, bool value)
        {
            element.SetValue(IsErrorProperty, value);
        }

        #endregion

        #region Приватные статичные методы

        /// <summary>
        /// Событие срабатывает при установке монитора слежки за <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void OnMonitorPasswordChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PasswordBox? passwordBox = obj as PasswordBox;

            if (passwordBox == null)
            {
                return;
            }

            passwordBox.PasswordChanged -= OnPasswordChanged;

            if ((bool)e.NewValue)
            {
                SetHasText(passwordBox);
                passwordBox.PasswordChanged += OnPasswordChanged;
            }
        }

        /// <summary>
        /// Событие срабатывает каждый раз при изменении <see cref="PasswordBox"/>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            SetHasText((PasswordBox)sender);
        }

        #endregion
    }
}