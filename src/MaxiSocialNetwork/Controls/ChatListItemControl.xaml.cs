﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Логика взаимодействия для ChatListItemControl.xaml
    /// </summary>
    public partial class ChatListItemControl : UserControl
    {
        #region Публичные статичные свойства

        /// <summary>
        /// Свойство, определяющее задний фон элемента
        /// </summary>
        public static readonly DependencyProperty BackgroundProperty =
            DependencyProperty.RegisterAttached("Background", typeof(SolidColorBrush), typeof(ChatListItemControl), new UIPropertyMetadata((SolidColorBrush) new BrushConverter().ConvertFrom("#FFF")));

        #endregion

        #region Публичные свойства

        /// <summary>
        /// Свойство, возвращающее значение свойства <see cref="BackgroundProperty"/>
        /// </summary>
        public SolidColorBrush Background
        {
            get => (SolidColorBrush)GetValue(BackgroundProperty);
            set => SetValue(BackgroundProperty, value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ChatListItemControl()
        {
            InitializeComponent();
        }

        #endregion
    }
}
