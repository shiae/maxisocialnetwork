﻿using System.Windows.Controls;
using System.Windows.Markup;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Логика взаимодействия для LoginPage.xaml
    /// </summary>
    public partial class LoginPage : BasePage<LoginPage, LoginViewModel>, IComponentConnector
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// При фокусе по элементу <see cref="TextBox"/> убирает ошибку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TextBox_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            TextBox? element = sender as TextBox;

            if (element != null)
            {
                TextBoxAttachedProperty.SetIsError(element, false);
            }
        }

        /// <summary>
        /// При фокусе по элементу <see cref="PasswordBox"/> убирает ошибку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Password_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            PasswordBox? element = sender as PasswordBox;

            if (element != null)
            {
                PasswordBoxAttachedProperty.SetIsError(element, false);
            }
        }

    }
}