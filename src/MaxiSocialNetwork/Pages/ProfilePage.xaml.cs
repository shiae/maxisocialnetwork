﻿using System.Windows.Markup;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Логика взаимодействия для ProfilePage.xaml
    /// </summary>
    public partial class ProfilePage : BasePage<ProfilePage, ProfilePageViewModel>, IComponentConnector
    {
        public ProfilePage()
        {
            InitializeComponent();
        }
    }
}
