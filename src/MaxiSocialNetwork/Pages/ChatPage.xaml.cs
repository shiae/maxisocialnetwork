﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.IO;
using System.Windows.Media.Imaging;
using System;
using System.Threading;
using Timer = System.Threading.Timer;
using Path = System.IO.Path;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Логика взаимодействия для ChatPage.xaml
    /// </summary>
    public partial class ChatPage : BasePage<ChatPage, ChatPageViewModel>, IComponentConnector
    {
        #region Приватные поля

        /// <summary>
        /// Используется для запуска таймера для Popup
        /// </summary>
        private Timer? _filterTimer;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ChatPage()
        {
            InitializeComponent();
        }

        #endregion

        #region Методы ChatPage

        /// <summary>
        /// Используется для включения Popup при наведении на кнопку
        /// </summary>
        private void StickerButton_MouseEnter(object sender, MouseEventArgs e)
        {
            StickerPopup.IsOpen = true;
            TxtSendMessage.Focus();
        }
        
        /// <summary>
        /// Используется для отключения Popup при отводе мышки с Popup
        /// </summary>
        private void StickerButton_MouseLeave(object sender, MouseEventArgs e)
        {
            _filterTimer?.Dispose();
            _filterTimer = new Timer((object? state) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (!StickerPopup.IsMouseOver)
                    {
                        StickerPopup.IsOpen = false;
                    }
                });
            }, 10, 500, Timeout.Infinite);
        }

        /// <summary>
        /// Используется для отключения Popup при отводе мышки с Popup
        /// </summary>
        private void StickerPopup_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!StickerPopup.IsMouseOver)
            {
                StickerPopup.IsOpen = false;
            }
        }

        #endregion

        #region Методы BasePage

        /// <summary>
        /// Инициализирует экземпляр <see cref="Page"/> асинхронным способом
        /// </summary>
        /// <returns>Экземпляр класса <see cref="Page"/></returns>
        public override async Task<ChatPage> InitializeAsync()
        {
            // Путь к стикерам
            string imagesPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", "Stickers");

            // Получаем список файлов из папки с изображениями стикеров
            string[] stickerFiles = Directory.GetFiles(imagesPath, "Sticker*.png");

            // Создаем стикеры и добавляем их в StackPanel
            foreach (string stickerFile in stickerFiles)
            {
                FileInfo fileInfo = new FileInfo(stickerFile);

                StickerPopupContaner.Items.Add(new FileDTO()
                {
                    Data = File.ReadAllBytes(stickerFile),
                    FileName = fileInfo.Name,
                    Extension = fileInfo.Extension,
                    Width = 100,
                    Height = 100
                });
            }

            await base.InitializeAsync();

            return this;
        }

        #endregion
    }
}
