﻿using System.Threading.Tasks;
using System.Windows.Controls;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Базовый класс для создания страниц, хранящий ViewModel
    /// </summary>
    public abstract class BasePage : UserControl
    {
        #region Публичные свойства

        /// <summary>
        /// ViewModel конкретной страницы
        /// </summary>
        public object? ViewModel { get; set; }

        #endregion
    }

    /// <summary>
    /// Базовый класс для создания страниц
    /// </summary>
    /// <typeparam name="VM">ViewModel конкретной страницы</typeparam>
    public class BasePage<Page, VM> : BasePage, IAsyncInitializer<Page>
        where Page : BasePage<Page, VM>, new()
        where VM : BaseViewModel<VM>, new()
    {
        #region Методы

        /// <summary>
        /// Инициализирует экземпляр <see cref="Page"/> асинхронным способом
        /// </summary>
        /// <returns>Экземпляр класса <see cref="Page"/></returns>
        public virtual async Task<Page> InitializeAsync()
        {
            ViewModel = await IAsyncInitializer<VM>.CreateAsync();
            DataContext = ViewModel;
            return (Page)this;
        }

        #endregion 
    }
}
