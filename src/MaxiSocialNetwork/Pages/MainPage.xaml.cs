﻿using System.Windows.Markup;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Логика взаимодействия для MainPage.xaml
    /// </summary>
    public partial class MainPage : BasePage<MainPage, MainViewModel>, IComponentConnector
    {
        public MainPage()
        {
            InitializeComponent();
        }
    }
}
