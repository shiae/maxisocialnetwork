﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Расширения для Expression
    /// </summary>
    public static class ExpressionExtension
    {
        /// <summary>
        /// Позволяет скомпилировать и получить свойство из Expression
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <returns>Свойство</returns>
        public static T GetPropertyValue<T>(this Expression<Func<T>> expression)
        {
            return expression.Compile().Invoke();
        }

        /// <summary>
        /// Позволяет установить значение свойству в Expression
        /// </summary>
        /// <param name="expression">Expression</param>
        /// <param name="value">Свойство</param>
        public static void SetPropertyValue<T>(this Expression<Func<T>> expression, T value)
        {
            var memberExpression = expression.Body as MemberExpression;
            var propertyInfo = memberExpression?.Member as PropertyInfo;
            var target = Expression.Lambda(memberExpression?.Expression).Compile().DynamicInvoke();
            propertyInfo?.SetValue(target, value);
        }
    }

}
