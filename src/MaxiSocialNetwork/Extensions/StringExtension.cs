﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Расширяющий класс для класса <see cref="string"/>
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// Добавляет к каждой строке метод Hash, которые хеширует строку
        /// </summary>
        /// <param name="str">Строка, которая будет захеширована</param>
        /// <returns>Захешированную строку</returns>
        public static string Hash(this string str)
        {
            using var sha256 = SHA256.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            byte[] hash = sha256.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }
    }
}
