﻿using System.IO;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace MaxiSocialNetwork
{
    public static class ImageExtennsion
    {
        public static byte[] ToByteArray(this Image image)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                BitmapEncoder encoder = new PngBitmapEncoder(); // Используйте JpegBitmapEncoder для сохранения в JPEG формате
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)image.Source));
                encoder.Save(memoryStream);

                return memoryStream.ToArray();
            }
        }
    }
}
