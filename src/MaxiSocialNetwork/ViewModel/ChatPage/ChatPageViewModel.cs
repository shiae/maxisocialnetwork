﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Application = System.Windows.Application;
using File = System.IO.File;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// ViewModel для страницы с чатом <see cref="ChatPage"/>
    /// </summary>
    public class ChatPageViewModel : BaseViewModel<ChatPageViewModel>
    {
        #region Приватные поля

        /// <summary>
        /// Используется для работы с пользователями
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        /// Используется для работы с сообщениями
        /// </summary>
        private readonly IMessageService _messageService;

        /// <summary>
        /// Используется для реализации задержки при фильтрации
        /// </summary>
        private Timer? _filterTimer;

        /// <summary>
        /// Используется для маппинга сущности <see cref="UserChatDTO"/>
        /// </summary>
        private readonly UserChatMapper _userChatMapper;

        /// <summary>
        /// Используется для маппинга сущности <see cref="MessageDTO"/>
        /// </summary>
        private readonly MessageMapper _messageMapper;

        #endregion

        #region Свойства

        /// <summary>
        /// Максимальная ширина отправленной картинки
        /// </summary>
        public double MaxSendImageWidth => 500;

        /// <summary>
        /// Максимальная высота отправленной картинки
        /// </summary>
        public double MaxSendImageHeight => 500;

        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public UserDTO? UserWithSelectedChat
        {
            get => Get<UserDTO>();
            set => Set(value);
        }

        /// <summary>
        /// Список чатов
        /// </summary>
        public ObservableCollection<ChatListItemViewModel> ChatList
        {
            get => Get<ObservableCollection<ChatListItemViewModel>>()!;
            set
            {
                Set(value);
                ChatListCollectionView = CollectionViewSource.GetDefaultView(value);
                ChatListCollectionView.SortDescriptions.Add(new SortDescription("MessageDate", ListSortDirection.Descending));
            }
        }

        /// <summary>
        /// Используется для отображения списка чатов <see cref="ChatList"/>
        /// </summary>
        public ICollectionView ChatListCollectionView
        {
            get => Get<ICollectionView>()!;
            set => Set(value);
        }

        /// <summary>
        /// Список сообщений
        /// </summary>
        public ObservableCollection<ChatMessageItemViewModel> MessageListOfCurrentChat
        {
            get => Get<ObservableCollection<ChatMessageItemViewModel>>()!;
            set
            {
                Set(value);

                if (value.Count >= 1)
                {
                    SelectedMessage = value[value.Count - 1];
                }
                else
                {
                    SelectedMessage = null;
                }

                MessageListOfCurrentChatCollectionView = CollectionViewSource.GetDefaultView(value);
            }
        }

        /// <summary>
        /// Используется для отображения списка чатов <see cref="MessageListOfCurrentChat"/>
        /// </summary>
        public ICollectionView MessageListOfCurrentChatCollectionView
        {
            get => Get<ICollectionView>()!;
            set => Set(value);
        }

        /// <summary>
        /// True - если список чатов все еще подгружается
        /// </summary>
        public bool ChatListIsLoading
        {
            get => Get<bool>();
            set
            {
                Set(value);
                IsEmptyChatList = IsEmptyChatList = value ? false : (ChatList.Count == 0);
            }
        }

        /// <summary>
        /// True - если список сообщений все еще подгружается
        /// </summary>
        public bool MessageListIsLoading
        {
            get => Get<bool>();
            set => Set(value);
        }
        
        /// <summary>
        /// Текущее сообщение
        /// </summary>
        public object? SelectedMessage
        {
            get => Get<object>();
            set
            {
                Set(value);

                if (value != null)
                {
                    ChatMessageItemViewModel chatMessageItemViewModel = (ChatMessageItemViewModel) value;
                    CanEditMessage = chatMessageItemViewModel.SentByCurrentUser;
                    SelectedTextMessage = chatMessageItemViewModel.Files?.Count <= 0;
                }
            }
        }

        /// <summary>
        /// True - выбрано не тектстовое сообщение
        /// </summary>
        public bool SelectedTextMessage
        {
            get => Get<bool>();
            set => Set(value);
        }
        
        /// <summary>
        /// True - могу менять сообщение
        /// </summary>
        public bool CanEditMessage
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Выбранный чат в списке чатов
        /// </summary>
        public object? SelectedChat
        {
            get => Get<object>();
            set
            {
                Set(value);
                HasSelectedChat = value != null;

                if (value == null)
                {
                    MessageListOfCurrentChat = new ObservableCollection<ChatMessageItemViewModel>();
                }
            }
        }

        /// <summary>
        /// Является ли список чатов пустым
        /// </summary>
        public bool IsEmptyChatList
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// True - имеется выбранный чат
        /// </summary>
        public bool HasSelectedChat
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// True - имеет чат без сообщений
        /// </summary>
        public bool HasChatWithoutMessages
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Содержит текст сообщения, которое в строке ввода для отправки
        /// </summary>
        public string? MessageText
        {
            get => Get<string>();
            set
            {
                Set(value);
                HasMessageText = !string.IsNullOrEmpty(value);
            }
        }

        /// <summary>
        /// True - имеет текст в строке ввода текста
        /// </summary>
        public bool HasMessageText
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Хранит выбранный стикер
        /// </summary>
        public object? SelectedSticker
        {
            get => Get<object>();
            set => Set(value);
        }

        /// <summary>
        /// Хранит в себе сообщение, которое мы изменяем
        /// </summary>
        public ChatMessageItemViewModel? EditedMessage
        {
            get => Get<ChatMessageItemViewModel>();
            set
            {
                Set(value);
                bool hasEditedMessage = value != null;
                IsVisibleBtnCancelEditedMessage = hasEditedMessage;
                ContentButton = hasEditedMessage ? "Изменить" : "Отправить";
                MessageText = hasEditedMessage ? MessageText : String.Empty;
            }
        }

        /// <summary>
        /// True - видно кнопку для отмены изменения сообщения
        /// </summary>
        public bool IsVisibleBtnCancelEditedMessage
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Контент кнопки
        /// </summary>
        public string? ContentButton
        {
            get => Get<string>();
            set => Set(value);
        }

        #endregion

        #region Команды

        /// <summary>
        /// Используется при выборе чата
        /// </summary>
        public ICommand SelectChatCommand { get; set; }

        /// <summary>
        /// Используется при поиске беседы в списке бесед
        /// </summary>
        public ICommand FilterChatsCommand { get; set; }

        /// <summary>
        /// Используется при клике на кнопку по работе с сообщениями
        /// </summary>
        public ICommand ButtonMessageClickCommand { get; set; }

        /// <summary>
        /// Используется при клике правой кнопкой мыши на элемент ListBox
        /// </summary>
        public ICommand SetEditedMessageCommand { get; set; }

        /// <summary>
        /// Копирование сообщения в буфер обмена
        /// </summary>
        public ICommand CopyMessageCommand { get; set; }
        
        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        public ICommand DeleteMessageCommand { get; set; }

        /// <summary>
        /// Используеься для отправки изображения в чате
        /// </summary>
        public ICommand SendImageCommand { get; set; }

        /// <summary>
        /// Используется для отмены изменения сообщения
        /// </summary>
        public ICommand CancelEditMessageCommand { get; set; }

        /// <summary>
        /// Используется при отправке стикера
        /// </summary>
        public ICommand SendStickerCommand { get; set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ChatPageViewModel()
        {
            _userService = IoC.Get<IUserService>();
            _messageService = IoC.Get<IMessageService>();
            _userChatMapper = IoC.Get<UserChatMapper>();
            _messageMapper = IoC.Get<MessageMapper>();

            SelectChatCommand = new AsyncRelayCommand(async () => await OnChatSelected());
            CopyMessageCommand = new AsyncRelayCommand(async () => await OnCopyMessage());
            DeleteMessageCommand = new AsyncRelayCommand(async () => await OnDeleteMessage());
            ButtonMessageClickCommand = new AsyncRelayCommand(async () => await OnButtonMessageClick());
            SetEditedMessageCommand = new AsyncRelayCommand(async () => await OnSetEditedMessage());
            SendImageCommand = new AsyncRelayCommand(async () => await OnSendImage());
            CancelEditMessageCommand = new AsyncRelayCommand(async () => await OnCancelEditMessage());
            FilterChatsCommand = new AsyncRelayParameterizedCommand(async (object? parameter) => await OnFilterChats(parameter));
            SendStickerCommand = new AsyncRelayParameterizedCommand(async (object? parameter) => await OnSendSticker(parameter));
            ChatList = new ObservableCollection<ChatListItemViewModel>();
            MessageListOfCurrentChat = new ObservableCollection<ChatMessageItemViewModel>();
            EditedMessage = null;

            UserHubConnection userHubConnection = IoC.Get<UserHubConnection>();
            userHubConnection.HubConnection.On<MessageDTO?>("ReceiveMessage", OnReceiveMessage);
            userHubConnection.HubConnection.On<EditMessageDTO?>("ChangedMessage", OnChangedMessage);
            userHubConnection.HubConnection.On<int>("UserConnected", OnUserConnected);
            userHubConnection.HubConnection.On<int>("UserDisconnected", OnUserDisconnected);
            userHubConnection.HubConnection.On<UserDTO>("UserProfileChanged", OnUserProfileChanged);
        }

        #endregion

        #region Методы

        /// <summary>
        /// Используется для ассинхронной инициализации экземпляра текущего класса
        /// </summary>
        /// <returns>Экземпляр текущего класса</returns>
        public override async Task<ChatPageViewModel> InitializeAsync()
        {
            await RunCommand(() => ChatListIsLoading, async () =>
            {
                ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                List<UserChatDTO> chatList = await _userService.GetChatList(appVM?.CurrentUser?.Login);
                ChatList = new ObservableCollection<ChatListItemViewModel>(_userChatMapper.ToChatListItemViewModel(chatList));
            });

            return this;
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Вызывается при выборе чата в списке чатов
        /// </summary>
        private async Task OnChatSelected()
        {
            if (SelectedChat is ChatListItemViewModel selectedChat)
            {
                HasChatWithoutMessages = true;

                await RunCommand(() => MessageListIsLoading, async () =>
                {
                    if (selectedChat.ConversationId != null)
                    {
                        List<MessageDTO> messages = await _messageService.GetAllByConversationId((int) selectedChat.ConversationId);
                        List<ChatMessageItemViewModel> chatMessageItemViewModels = messages.Select(item => _messageMapper.ToChatMessageItemViewModel(item)).ToList();
                        MessageListOfCurrentChat = new ObservableCollection<ChatMessageItemViewModel>(chatMessageItemViewModels);
                    }
                    else
                    {
                        MessageListOfCurrentChat = new ObservableCollection<ChatMessageItemViewModel>();
                    }

                    await ReadMessage(selectedChat);
                    await SetUserWithSelectedChat(selectedChat);
                    HasChatWithoutMessages = SelectedChat != null && !MessageListOfCurrentChatCollectionView.IsEmpty;
                });
            }
        }

        /// <summary>
        /// Используется для чтения непрочитанного сообщения
        /// </summary>
        private async Task ReadMessage(ChatListItemViewModel selectedChat)
        {
            if (selectedChat.HasUnreadMessages)
            {
                selectedChat.HasUnreadMessages = false;
                await SetUnreadMessage((int)selectedChat.ConversationId!, selectedChat.HasUnreadMessages);
            }
        }

        /// <summary>
        /// Используется для установки пользователя текущего выбранного чата
        /// </summary>
        private async Task SetUserWithSelectedChat(ChatListItemViewModel selectedChat)
        {
            await Application.Current.Dispatcher.Invoke(async () =>
            {
                UserDTO? userWithSelectedChat = await _userService.GetUserById(selectedChat.UserId);

                if (userWithSelectedChat != null)
                {
                    userWithSelectedChat.ProfileRgbImage = ChatList
                        .Where(chat => chat.UserId == userWithSelectedChat.Id)
                        .FirstOrDefault()
                        ?.ProfileRgbImage;
                    userWithSelectedChat.HasProfileImage = userWithSelectedChat.ProfileImage != null;
                    userWithSelectedChat.Initials = $"{userWithSelectedChat?.FirstName![0]} {userWithSelectedChat?.LastName![0]}";
                    UserWithSelectedChat = userWithSelectedChat;
                }
            });
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <returns></returns>
        private async Task EditMessage()
        {
            if (EditedMessage != null && EditedMessage.Messsage != null && SelectedChat is ChatListItemViewModel selectedChat)
            {
                EditedMessage.Messsage = MessageText;
                await _messageService.EditMessage(_messageMapper.ToEditMessageDTO(EditedMessage, selectedChat.ConversationId));
                EditedMessage = null;
            }
        }

        /// <summary>
        /// Вызывается тогда, когда любой пользователь в системе подключается
        /// </summary>
        private async Task OnUserConnected(int userId)
        {
            await Task.Run(async () =>
            {
                ChatListItemViewModel? foundChatListItemViewModel = null;

                foreach (ChatListItemViewModel chatListItemViewModel in ChatListCollectionView)
                {
                    if (chatListItemViewModel.UserId == userId)
                    {
                        foundChatListItemViewModel = chatListItemViewModel;
                        break;
                    }
                }

                if (foundChatListItemViewModel != null)
                {
                    await Application.Current.Dispatcher.Invoke(() =>
                    {
                        foundChatListItemViewModel.IsOnline = true;
                        return Task.CompletedTask;
                    });
                }
                else
                {
                    UserDTO? user = await _userService.GetUserById(userId);
                    ChatList.Add(_userChatMapper.ToChatListItemViewModel(user));
                }
            });

        }

        /// <summary>
        /// Вызывается тогда, когда любой пользователь в системе отключается
        /// </summary>
        private async Task OnUserDisconnected(int userId)
        {
            await Task.Run(async () =>
            {
                foreach (ChatListItemViewModel chatListItemViewModel in ChatList)
                {
                    if (chatListItemViewModel.UserId == userId)
                    {
                        await Application.Current.Dispatcher.Invoke(() =>
                        {
                            chatListItemViewModel.IsOnline = false;
                            return Task.CompletedTask;
                        });

                        break;
                    }
                }
            });
        }

        /// <summary>
        /// Используется для отправки сообщения человеку
        /// </summary>
        /// <param name="parameter">Содержание сообщения</param>
        private async Task SendMessage()
        {
            if (SelectedChat is ChatListItemViewModel selectedChat && (!string.IsNullOrEmpty(MessageText) || SelectedSticker != null))
            {
                SendMessageDTO sendMessageDTO = new SendMessageDTO()
                {
                    RecipientId = selectedChat.UserId,
                    SenderId = (int)IoC.Get<ApplicationViewModel>()?.CurrentUser?.Id!,
                    ConversationId = selectedChat.ConversationId,
                    Files = new()
                };

                if (SelectedSticker is FileDTO selectedSticker)
                {
                    sendMessageDTO.Files.Add(selectedSticker);
                    SelectedSticker = null;
                }
                else
                {
                    sendMessageDTO.Text = MessageText;
                    MessageText = string.Empty;
                }

                await _userService.SendMessage(sendMessageDTO);
            }
        }

        /// <summary>
        /// Событие, которое вызывается при получении сообщения
        /// </summary>
        /// <param name="messageDTO"></param>
        private async Task OnReceiveMessage(MessageDTO? messageDTO)
        {
            if (messageDTO != null)
            {
                await Task.Run(async () =>
                {
                    await Application.Current.Dispatcher.Invoke(async () =>
                    {
                        ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();

                        if (SelectedChat is ChatListItemViewModel selectedChat)
                        {
                            if (messageDTO.ConversationId == selectedChat.ConversationId ||
                                (messageDTO.SenderId == selectedChat.UserId && appVM?.CurrentUser?.Id == messageDTO.RecipientId) ||
                                (messageDTO.SenderId == appVM?.CurrentUser?.Id && selectedChat.UserId == messageDTO.RecipientId))
                            {
                                MessageListOfCurrentChat.Add(_messageMapper.ToChatMessageItemViewModel(messageDTO));
                                HasChatWithoutMessages = true;
                            }
                        }

                        await UpdateInformationOfChatListItem(messageDTO);
                    });
                });
            }
        }

        /// <summary>
        /// Используется для обновления информации элемента из списка чатов
        /// </summary> 
        /// <param name="chatListItemView"></param>
        private async Task UpdateInformationOfChatListItem(MessageDTO messageDTO)
        {
            bool hasUnreadMessages = true;

            if (SelectedChat is ChatListItemViewModel selectedChat)
            {
                if (selectedChat.ConversationId != 0 || selectedChat.ConversationId != null)
                {
                    ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();

                    if (messageDTO.SenderId == appVM?.CurrentUser?.Id)
                    {
                        hasUnreadMessages = false;
                    }
                    else
                    {
                        hasUnreadMessages = !(messageDTO.SenderId == selectedChat.UserId && appVM?.CurrentUser?.Id == messageDTO.RecipientId);
                        await SetUnreadMessage(messageDTO.ConversationId, hasUnreadMessages);
                    }
                }
            }

            var foundChat = ChatList.Where(item => item.UserId == messageDTO.SenderId || item.UserId == messageDTO.RecipientId).FirstOrDefault();

            if (foundChat != null)
            {
                foundChat.MessageId = messageDTO.Id;
                foundChat.MessageDate = messageDTO.SentAt;
                foundChat.HasUnreadMessages = hasUnreadMessages;
                foundChat.Message = messageDTO.Text ?? "Отправлено медиа-сообщение";
                foundChat.SendSticker = string.IsNullOrEmpty(messageDTO.Text);
                foundChat.ConversationId = messageDTO.ConversationId;
            }

            ChatListCollectionView.Refresh();
        }

        /// <summary>
        /// Используется для получения уведомления о том, что какое то сообщение изменилось в чате
        /// </summary>
        /// <param name="messageDTO"></param>
        private async Task OnChangedMessage(EditMessageDTO? editMessageDTO)
        {
            if (editMessageDTO is null)
            {
                return;
            }

            await RunCommand(() => MessageListIsLoading, async () =>
            {
                await Application.Current.Dispatcher.Invoke(async () =>
                {
                    if (editMessageDTO.Flag == 0)
                    {
                        var foundMessage = MessageListOfCurrentChat.Where(item => item.Id == editMessageDTO.Id).FirstOrDefault();

                        if (foundMessage != null)
                        {
                            foundMessage.Messsage = editMessageDTO.Message;
                        }

                        ChatListItemViewModel? chatListItem = ChatList.Where(item => item.MessageId == editMessageDTO.Id).FirstOrDefault();

                        if (chatListItem != null)
                        {
                            chatListItem.Message = editMessageDTO.Message;
                        }
                    }
                    else if (editMessageDTO.Flag == 1)
                    {
                        ChatMessageItemViewModel? foundMessage = MessageListOfCurrentChat.Where(item => item.Id == editMessageDTO.Id).FirstOrDefault();

                        if (foundMessage != null)
                        {
                            MessageListOfCurrentChat.Remove(foundMessage);
                        }

                        ChatListItemViewModel? chatListItem = ChatList.Where(item => item.ConversationId == editMessageDTO.ConversationId).FirstOrDefault();

                        if (chatListItem != null)
                        {
                            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                            GetChatListItemByCurrentUserAndParticipateUserIdDTO getChatListItemDTO = new()
                            {
                                CurrentUserId = (int)appVM?.CurrentUser?.Id!,
                                ParticipateUserId = chatListItem.UserId
                            };
                            UserChatDTO? userChatDTO = await _userService.GetChatListItemByCurrentUserAndParticipateUserId(getChatListItemDTO);

                            if (userChatDTO == null)
                            {
                                chatListItem.MessageId = null;
                                chatListItem.Message = null;
                                chatListItem.SendSticker = false;
                                chatListItem.IsNewChat = true;
                                chatListItem.HasUnreadMessages = false;
                            }
                            else
                            {
                                chatListItem.MessageId = userChatDTO.MessageId;
                                chatListItem.Message = userChatDTO.Message ?? "Отправлено медиа-сообщение";
                                chatListItem.MessageDate = userChatDTO.MessageDate;
                                chatListItem.SendSticker = string.IsNullOrEmpty(userChatDTO.Message);
                                chatListItem.HasUnreadMessages = false;
                            }

                            await SetUnreadMessage((int)chatListItem.ConversationId!, chatListItem.HasUnreadMessages);
                        }
                    }

                    HasChatWithoutMessages = !MessageListOfCurrentChatCollectionView.IsEmpty;
                });
            });
        }

        /// <summary>
        /// Используется для фильтрации списка бесед при вводе текста
        /// </summary>
        /// <param name="parameter">Текущее значение ввода</param>
        private Task OnFilterChats(object? parameter)
        {
            if (parameter != null)
            {
                string? userInput = parameter.ToString();

                _filterTimer?.Dispose();
                _filterTimer = new Timer(async (object? state) =>
                {
                    await RunCommand(() => ChatListIsLoading, async () =>
                    {
                        Predicate<object>? predicate;

                        if (!string.IsNullOrEmpty(state?.ToString()))
                        {
                            predicate = (item) =>
                            {
                                if (item is ChatListItemViewModel chat)
                                {
                                    return chat.FullName.ToLower().Contains(state?.ToString()?.ToLower());
                                }
                                else
                                {
                                    return false;
                                }
                            };
                        }
                        else
                        {
                            predicate = null;
                        }

                        await Application.Current.Dispatcher.Invoke(() =>
                        {
                            ChatListCollectionView.Filter = predicate;
                            ChatListCollectionView.Refresh();
                            return Task.CompletedTask;
                        });
                    });

                    HasChatWithoutMessages = !MessageListOfCurrentChatCollectionView.IsEmpty;
                    IsEmptyChatList = ChatListCollectionView.IsEmpty;

                }, userInput, 500, Timeout.Infinite);
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Используется для обновления интерфейса, когда пользователь изменился
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public async Task OnUserProfileChanged(UserDTO userDTO)
        {
            await Application.Current.Dispatcher.Invoke(async () =>
            {
                var foundChat = ChatList.Where(item => item.UserId == userDTO.Id).FirstOrDefault();

                if (foundChat != null)
                {
                    foundChat.ProfileImage = userDTO.ProfileImage;
                    foundChat.FirstName = userDTO.FirstName;
                    foundChat.LastName = userDTO.LastName;

                    if (UserWithSelectedChat != null && UserWithSelectedChat.Id == userDTO.Id)
                    {
                        UserDTO? user = await _userService.GetUserById((int)userDTO.Id!);
                        UserWithSelectedChat.FirstName = user.FirstName;
                        UserWithSelectedChat.LastName = user.LastName;
                        UserWithSelectedChat.Email = user.Email;
                        UserWithSelectedChat.DateOfBirth = user.DateOfBirth;
                        UserWithSelectedChat.Phone = user.Phone;
                        UserWithSelectedChat.Website = user.Website;
                        UserWithSelectedChat.Address = user.Address;
                        UserWithSelectedChat.School = user.School;
                        UserWithSelectedChat.Description = user.Description;
                        UserWithSelectedChat.ProfileImage = user.ProfileImage;
                        UserWithSelectedChat.HasProfileImage = user.ProfileImage != null;
                        OnPropertyChanged(nameof(UserWithSelectedChat));
                    }

                    ChatListCollectionView.Refresh();
                    HasChatWithoutMessages = !MessageListOfCurrentChatCollectionView.IsEmpty;
                    IsEmptyChatList = ChatListCollectionView.IsEmpty;
                }
            });
        }

        /// <summary>
        /// Используется для выбора операции при клике на кнопку по работе с сообщениями
        /// </summary>
        /// <returns></returns>
        private async Task OnButtonMessageClick()
        {
            if (EditedMessage != null)
            {
                await EditMessage();
            }
            else
            {
                await SendMessage();
            }
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private Task OnSetEditedMessage()
        {
            if (SelectedMessage is ChatMessageItemViewModel selectedMessage)
            {
                if (!string.IsNullOrEmpty(selectedMessage.Messsage))
                {
                    MessageText = selectedMessage.Messsage ?? string.Empty;
                    EditedMessage = selectedMessage;
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Используется для копирования сообщения в буфер обмена
        /// </summary>
        private Task OnCopyMessage()
        {
            if (SelectedMessage is ChatMessageItemViewModel selectedMessage)
            {
                if (!string.IsNullOrEmpty(selectedMessage.Messsage))
                {
                    Clipboard.SetText(selectedMessage.Messsage);
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        private Task OnDeleteMessage()
        {
            MessageBoxResult result = MessageBox.Show("Вы точно хотите удалить сообщение?", "Удаление сообщения", MessageBoxButton.YesNo, MessageBoxImage.Question);
            
            if (result == MessageBoxResult.Yes)
            {
                if (SelectedMessage is ChatMessageItemViewModel selectedMessage)
                {
                    _messageService.DeleteMessage((int)selectedMessage.Id!);
                }
            }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Используется для установки статуса непрочитанному сообщению
        /// </summary>
        /// <param name="setUnreadMessageDTO"></param>
        /// <returns></returns>
        public async Task SetUnreadMessage(int conversationId, bool hasUnreadMessages)
        {
            // Запрос на сервер на то, что мы прочитали сообщение
            SetUnreadMessageDTO setUnreadMessageDTO = new SetUnreadMessageDTO()
            {
                ConversationId = conversationId,
                HasUnreadMessages = hasUnreadMessages
            };

            await _userService.EditHasUnreadMessageStatus(setUnreadMessageDTO);
        }

        /// <summary>
        /// Используется для отправки изображений в чате
        /// </summary>
        public async Task OnSendImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.bmp;*.jpg;*.jpeg,*.png)|*.bmp;*.jpg;*.jpeg;*.png";

            if (openFileDialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);

                if (fileInfo.Length > 5 * 1024 * 1024)
                {
                    MessageBox.Show("Выбранный файл слишком большой. Максимальный размер 5 МБ.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    byte[] image = await File.ReadAllBytesAsync(openFileDialog.FileName);

                    if (SelectedChat is ChatListItemViewModel selectedChat)
                    {
                        BitmapImage bitmapImage = new BitmapImage();
                        bitmapImage.BeginInit();
                        bitmapImage.UriSource = new Uri(fileInfo.FullName, UriKind.RelativeOrAbsolute);
                        bitmapImage.EndInit();

                        double widthRatio = MaxSendImageWidth / bitmapImage.Width;
                        double heightRatio = MaxSendImageHeight / bitmapImage.Height;
                        double scale = Math.Min(widthRatio, heightRatio);

                        FileDTO file = new FileDTO()
                        {
                            Data = image,
                            FileName = fileInfo.Name,
                            Extension = fileInfo.Extension,
                            Height = bitmapImage.Width * scale,
                            Width = bitmapImage.Height * scale
                        };

                        SendMessageDTO sendMessageDTO = new SendMessageDTO()
                        {
                            RecipientId = selectedChat.UserId,
                            SenderId = (int)IoC.Get<ApplicationViewModel>()?.CurrentUser?.Id!,
                            ConversationId = selectedChat.ConversationId,
                            Files = new List<FileDTO>() { file }
                        };

                        await _userService.SendMessage(sendMessageDTO);
                        HasChatWithoutMessages = true;
                    }
                }
             }
        }

        /// <summary>
        /// Используется для отмены изменения сообщения
        /// </summary>
        /// <returns></returns>
        private Task OnCancelEditMessage()
        {
            EditedMessage = null;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Используется для отправки стикера
        /// </summary>
        /// <param name="parameter">Стикер</param>
        private async Task OnSendSticker(object? parameter)
        {
            if (parameter is FileDTO)
            {
                SelectedSticker = parameter;
                await OnButtonMessageClick();
            }
        }

        #endregion
    }
}
