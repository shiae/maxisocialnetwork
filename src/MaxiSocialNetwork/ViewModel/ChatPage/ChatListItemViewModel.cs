﻿using System;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// ViewModel для каждого элемента <see cref="ChatListItemControl"/> в списке чатов <see cref="ChatListControl"/>
    /// </summary>
    public class ChatListItemViewModel : BaseViewModel<ChatListItemViewModel>
    {
        #region Свойства

        /// <summary>
        /// ID сообщения
        /// </summary>
        public int? MessageId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId
        {
            get => Get<int>();
            set => Set(value);
        }

        /// <summary>
        /// ID чата
        /// </summary>
        public int? ConversationId
        {
            get => Get<int>();
            set
            {
                Set(value);
                IsNewChat = value == null;
            }
        }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? FirstName
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string? LastName
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Имя + Фамилия
        /// </summary>
        public string FullName => $"{LastName} {FirstName}";

        /// <summary>
        /// Инициалы
        /// </summary>
        public string Initials => $"{FirstName[0]} {LastName[0]}";

        /// <summary>
        /// Последнее сообщение, отправленное в чате
        /// </summary>
        public string? Message
        {
            get => Get<string>();
            set
            {

                if (value?.Length > 31)
                {
                    Set(value.Substring(0, 31) + "...");
                }
                else
                {
                    Set(value);
                }
            }
        }

        /// <summary>
        /// Дата отправки последнего сообщения
        /// </summary>
        public DateTime? MessageDate
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        /// <summary>
        /// True - является новым чатом
        /// </summary>
        public bool IsNewChat
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// True, если сообщение новое
        /// </summary>
        public bool HasUnreadMessages
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Картинка пользователя
        /// </summary>
        public byte[]? ProfileImage
        {
            get => Get<byte[]?>();
            set
            {
                Set(value);
                HasProfileImage = value != null;
            }
        }

        /// <summary>
        /// RGB цвет картинки пользователя
        /// </summary>
        public string? ProfileRgbImage
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// True - если присутствует картинка
        /// </summary>
        public bool HasProfileImage
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// True - является ли содержанием последнего сообщение стикер
        /// </summary>
        public bool SendSticker
        {
            get => Get<bool>();
            set
            {
                Set(value);
                TextColor = value ? "#018833" : "#696969";
            }
        }

        /// <summary>
        /// Определяет цвет текста
        /// </summary>
        public string? TextColor
        {
            get => Get<string>();
            set => Set(value);
        }

        /// <summary>
        /// Является ли пользователь в сети
        /// </summary>
        public bool IsOnline
        {
            get => Get<bool>();
            set => Set(value);
        }

        #endregion
    }
}
