﻿using System;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    public class RelayCommand : ICommand
    {
        #region Приватные поля

        /// <summary>
        /// Действие, которое будет запускаться
        /// </summary>
        private Action _action;

        #endregion

        #region Публичный события

        /// <summary>
        /// Событие, которое вызывается когда <see cref="CanExecute(object)"/> изменяется
        /// </summary>
        public event EventHandler? CanExecuteChanged;

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public RelayCommand(Action action)
        {
            _action = action;
        }

        #endregion

        #region ICommand Методы

        /// <summary>
        /// RelayCommand может всегда выполняться
        /// </summary>
        /// <param name="parameter">Параметры для определения выполнения команды</param>
        /// <returns>Признак выполнения команды</returns>
        public bool CanExecute(object? parameter)
        {
            return true;
        }

        /// <summary>
        /// Выполняет команду Action
        /// </summary>
        /// <param name="parameter">Параметры для выполнения команды</param>
        public void Execute(object? parameter)
        {
            _action();
        }

        #endregion
    }
}
