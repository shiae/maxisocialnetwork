﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Команда без параметра
    /// </summary>
    public class AsyncRelayCommand : ICommand
    {
        #region Приватные поля

        /// <summary>
        /// Действие, которое будет запускаться
        /// </summary>
        private Func<Task> _action;

        #endregion

        #region Публичный события

        /// <summary>
        /// Событие, которое вызывается когда <see cref="CanExecute(object)"/> изменяется
        /// </summary>
        public event EventHandler? CanExecuteChanged;

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        public AsyncRelayCommand(Func<Task> action)
        {
            _action = action;
        }

        #endregion

        #region ICommand Методы

        /// <summary>
        /// RelayCommand может всегда выполняться
        /// </summary>
        /// <param name="parameter">Параметры для определения выполнения команды</param>
        /// <returns>Признак выполнения команды</returns>
        public bool CanExecute(object? parameter)
        {
            return true;
        }

        /// <summary>
        /// Выполняет команду Action
        /// </summary>
        /// <param name="parameter">Параметры для выполнения команды</param>
        public async void Execute(object? parameter)
        {
            await _action();
        }

        #endregion
    }
}
