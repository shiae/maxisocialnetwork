﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// ViewModel для каждого сообщения в персональном чате
    /// </summary>
    public class ChatMessageItemViewModel : BaseViewModel<ChatMessageItemViewModel>
    {
        /// <summary>
        /// ID сообщения
        /// </summary>
        public int? Id
        {
            get => Get<int>();
            set => Set(value);
        }

        /// <summary>
        /// Время отправки сообщения
        /// </summary>
        public DateTime? SentAt
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        /// <summary>
        /// Время изменения сообщения
        /// </summary>
        public DateTime? UpdatedAt
        {
            get => Get<DateTime>();
            set => Set(value);
        }

        /// <summary>
        /// Содержит текст сообщения
        /// </summary>
        public string? Messsage
        {
            get => Get<string>()!;
            set
            {
                Set(value);
                HasMessage = !string.IsNullOrEmpty(value);
            }
        }
        
        /// <summary>
        /// Хранит файлы сообщения
        /// </summary>
        public List<FileDTO>? Files
        {
            get => Get<List<FileDTO>>()!;
            set => Set(value);
        }

        /// <summary>
        /// Пользователь, который отправил сообщение
        /// </summary>
        public bool SentByCurrentUser
        {
            get => Get<bool>();
            set
            {
                Set(value);
                BackgroundMessageColor = value ? "#def1fd" : "#FFF";
                CornerRadius = value ? new CornerRadius(15, 15, 0, 15) : new CornerRadius(15, 15, 15, 0);
                HorizontalAlignment = value ? HorizontalAlignment.Right : HorizontalAlignment.Left;
            }
        }

        /// <summary>
        /// HEX цвет заднего фона для сообщения
        /// </summary>
        public string BackgroundMessageColor
        {
            get => Get<string>()!;
            set => Set(value);
        }

        /// <summary>
        /// Цвет текста
        /// </summary>
        public string ForegroundMessageColor
        {
            get => Get<string>()!;
            set => Set(value);
        }

        /// <summary>
        /// Закругление сообщения
        /// </summary>
        public CornerRadius CornerRadius
        {
            get => Get<CornerRadius>();
            set => Set(value);
        }

        /// <summary>
        /// Выравнивание сообщения по горизонтале 
        /// </summary>
        public HorizontalAlignment HorizontalAlignment
        {
            get => Get<HorizontalAlignment>();
            set => Set(value);
        }

        /// <summary>
        /// True - имеется сообщение
        /// </summary>
        public bool HasMessage
        {
            get => Get<bool>();
            set => Set(value);
        }
    }
}
