﻿using System;

namespace MaxiSocialNetwork
{
    public class ChatMessageItemViewModelDesign : ChatMessageItemViewModel
    {
        #region Singleton

        /// <summary>
        /// Экземпляр <see cref="ChatMessageItemViewModelDesign"/> модели
        /// </summary>
        public static ChatMessageItemViewModelDesign Instance = new ChatMessageItemViewModelDesign();

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        private ChatMessageItemViewModelDesign()
        {
            SentAt = DateTime.Now;
            Messsage = "Что движет человеком?";
            SentByCurrentUser = true;
        }

        #endregion
    }
}
