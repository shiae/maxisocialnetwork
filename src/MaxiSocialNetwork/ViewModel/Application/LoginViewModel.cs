﻿using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализованный ViewModel для <see cref="LoginPage"/> страницы
    /// </summary>
    public class LoginViewModel : BaseViewModel<LoginViewModel>
    {
        #region Приватные readonly поля

        /// <summary>
        /// Бизнес логника для реализации аутентификации
        /// </summary>
        private readonly IAuthenticationSerivce _authenticationService;

        #endregion

        #region Публичные свойства

        /// <summary>
        /// Свойство, показывающее, что алгоритм выполнения входа в систему еще отрабатывает
        /// </summary>
        public bool LoginIsRunning
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Свойство хранит введеный логин от пользователя
        /// </summary>
        public string? LoginProperty
        {
            get => Get<string?>();
            set => Set(value);
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда, которая используется для выполнения входа в систему
        /// </summary>
        public ICommand LoginCommand { get; set; }

        /// <summary>
        /// Команда, которая используется для перехода на страницу регистрации
        /// </summary>
        public ICommand GoToRegistrationPageCommand { get; set; }

        /// <summary>
        /// Команда - забыл пароль
        /// </summary>
        public ICommand ForgotPasswordCommand { get; set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public LoginViewModel()
        {
            this._authenticationService = IoC.Get<IAuthenticationSerivce>();
            this.LoginIsRunning = false;
            this.LoginCommand = new AsyncRelayParameterizedCommand(async (parameter) => await Login(parameter));
            this.GoToRegistrationPageCommand = new AsyncRelayCommand(async () => await GoToRegistrationPage());
            this.ForgotPasswordCommand = new AsyncRelayCommand(async () => await ForgotPassword());
        }

        #endregion

        #region Реализация команд

        /// <summary>
        /// Реализация команды <see cref="LoginCommand"/> для выполнения входа в систему
        /// </summary>
        /// <param name="parameter">The <see cref="SecureString"/> passed in from the view for the users password</param>
        public async Task Login(object parameter)
        {
            await RunCommand(() => LoginIsRunning, async () =>
            {
                try
                {
                    LoginPage? page = parameter as LoginPage;

                    if (!CheckErrorsBeforeLogin(page))
                    {
                        UserDTO user = new()
                        {
                            Login = LoginProperty,
                            Password = page?.Password?.Password?.Hash()
                        };

                        ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                        appVM.CurrentUser = await _authenticationService.LoginUser(user);
                        appVM.GoToPage(await IAsyncInitializer<MainPage>.CreateAsync());
                    }
                }
                catch (ApplicationException ex)
                {
                    ShowErrorMessage(ex.Message);
                }
            });
        }

        /// <summary>
        /// Реализация команда <see cref="RegistrationCommand"/> для перехода на страницу регистрации
        /// </summary>
        public async Task GoToRegistrationPage()
        {
            await RunCommand(() => LoginIsRunning, async () =>
            {
                IoC.Get<ApplicationViewModel>()
                   .GoToPage(await IAsyncInitializer<RegistrationPage>.CreateAsync());
            });
        }

        /// <summary>
        /// Реализация команда - забыл пароль
        /// </summary>
        public Task ForgotPassword()
        {
            MessageBox.Show("Обратитесь для восстановления пароля к системному администратору!", "Восстановление пароля", MessageBoxButton.OK, MessageBoxImage.Information);
            return Task.CompletedTask;
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Проверяет заполненость необходимых полей на форме регистрации, в случае незаполнения - показывает ошибку пользователю
        /// </summary>
        /// <param name="page">Страница, где проверяем поля необходимые</param>
        /// <returns>Признак, имеются ли ошибку на странице</returns>
        public bool CheckErrorsBeforeLogin(LoginPage? page)
        {
            StringBuilder errors = new StringBuilder();
            bool hasErrors = false;

            if (page != null)
            {
                if (string.IsNullOrEmpty(LoginProperty))
                {
                    TextBoxAttachedProperty.SetIsError(page.Login, true);
                    hasErrors = true;
                    errors.Append("Логин не может быть пустым").Append("\n");
                }

                if (string.IsNullOrEmpty(page.Password.Password))
                {
                    PasswordBoxAttachedProperty.SetIsError(page.Password, true);
                    hasErrors = true;
                    errors.Append("Пароль не может быть пустым").Append("\n");
                }

                if (errors.Length > 0 && hasErrors)
                {
                    ShowErrorMessage(errors.ToString());
                }
            }

            return hasErrors;
        }

        #endregion
    }
}
