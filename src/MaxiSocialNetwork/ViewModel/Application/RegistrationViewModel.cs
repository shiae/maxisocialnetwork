﻿using MaxiSocialNetworkChecker;
using MaxiSocialNetworkChecker.Exceptions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализация <see cref="BaseViewModel"/>
    /// </summary>
    public class RegistrationViewModel : BaseViewModel<RegistrationViewModel>
    {
        #region Приватные readonly поля

        /// <summary>
        /// Бизнес логника для реализации аутентификации
        /// </summary>
        private readonly IAuthenticationSerivce _authenticationService;

        /// <summary>
        /// Используется для проверки пароля
        /// </summary>
        private readonly PasswordChecker _passwordChecker;

        /// <summary>
        /// Используется для проверки логина
        /// </summary>
        private readonly LoginChecker _loginChecker;

        #endregion

        #region Публичные свойства

        /// <summary>
        /// Свойство, показывающее, что алгоритм выполнения входа в систему еще отрабатывает
        /// </summary>
        public bool RegistrationIsRunning
        {
            get => Get<bool>();
            set => Set(value);
        }

        /// <summary>
        /// Свойство, хранящее логин пользователя
        /// </summary>
        public string? LoginProperty
        {
            get => Get<string?>();
            set => Set(value);
        }

        /// <summary>
        /// Свойство, хранящее имя пользователя
        /// </summary>
        public string? FirstNameProperty
        {
            get => Get<string?>();
            set => Set(value);
        }

        /// <summary>
        /// Свойство, хранящее фамилию пользователя
        /// </summary>
        public string? LastNameProperty
        {
            get => Get<string?>();
            set => Set(value);
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда, которая используется для перехода на страницу регистрации
        /// </summary>
        public ICommand RegistrationCommand { get; set; }

        /// <summary>
        /// Команда, которая используется для переключения на страницу логина
        /// </summary>
        public ICommand GoToBackLoginPageCommand { get; set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public RegistrationViewModel()
        {
            this._authenticationService = IoC.Get<IAuthenticationSerivce>();
            this._passwordChecker = IoC.Get<PasswordChecker>();
            this._loginChecker = IoC.Get<LoginChecker>();
            this.RegistrationIsRunning = false;
            this.RegistrationCommand = new AsyncRelayParameterizedCommand(async (parameter) => await Registration(parameter));
            this.GoToBackLoginPageCommand = new AsyncRelayCommand(async () => await GoToBackLoginPage());
        }

        #endregion

        #region Реализация команд

        /// <summary>
        /// Реализация команды <see cref="LoginCommand"/> для выполнения входа в систему
        /// </summary>
        /// <param name="parameter">The <see cref="SecureString"/> passed in from the view for the users password</param>
        public async Task Registration(object parameter)
        {
            await RunCommand(() => RegistrationIsRunning, async () =>
            {
                try
                {
                    RegistrationPage? page = parameter as RegistrationPage;

                    if (!CheckErrorsBeforeRegistration(page))
                    {
                        UserDTO user = new()
                        {
                            FirstName = FirstNameProperty,
                            LastName = LastNameProperty,
                            Login = LoginProperty,
                            Password = page?.Password?.Password?.Hash()
                        };

                        await _authenticationService.RegisterUser(user);

                        IoC.Get<ApplicationViewModel>()
                           .GoToPage(await IAsyncInitializer<LoginPage>.CreateAsync());
                    }
                }
                catch (ApplicationException ex)
                {
                    ShowErrorMessage(ex.Message);
                }
            });
        }

        /// <summary>
        /// Реализация команда <see cref="RegistrationCommand"/> для перехода на страницу регистрации
        /// </summary>
        public async Task GoToBackLoginPage()
        {
            await RunCommand(() => RegistrationIsRunning, async () =>
            {
                ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                appVM.GoToPage(await IAsyncInitializer<LoginPage>.CreateAsync());
            });
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Проверяет заполненость необходимых полей на форме регистрации, в случае незаполнения - показывает ошибку пользователю
        /// </summary>
        /// <param name="page">Страница, где проверяем поля необходимые</param>
        /// <returns>Признак, имеются ли ошибку на странице</returns>
        public bool CheckErrorsBeforeRegistration(RegistrationPage? page)
        {
            StringBuilder errors = new StringBuilder();
            bool hasErrors = false;

            if (page != null)
            {
                if (string.IsNullOrEmpty(LoginProperty))
                {
                    TextBoxAttachedProperty.SetIsError(page.Login, true);
                    hasErrors = true;
                }

                try
                {
                    _loginChecker.Check(LoginProperty);
                }
                catch (BaseCheckerException ex)
                {
                    errors.Append(ex.Message).Append("\n");
                    hasErrors = true;
                }

                if (string.IsNullOrEmpty(FirstNameProperty))
                {
                    TextBoxAttachedProperty.SetIsError(page.FirstName, true);
                    hasErrors = true;
                    errors.Append("Имя не может быть пустым").Append("\n");
                }

                if (string.IsNullOrEmpty(LastNameProperty))
                {
                    TextBoxAttachedProperty.SetIsError(page.LastName, true);
                    hasErrors = true;
                    errors.Append("Фамилия не может быть пустой").Append("\n");
                }

                if (FirstNameProperty.Any(char.IsDigit))
                {
                    hasErrors = true;
                    errors.Append("В имени не могут быть цифры!").Append("\n");
                }

                if (LastNameProperty.Any(char.IsDigit))
                {
                    hasErrors = true;
                    errors.Append("В фамилии не могут быть цифры!").Append("\n");
                }

                if (string.IsNullOrEmpty(page.Password.Password))
                {
                    PasswordBoxAttachedProperty.SetIsError(page.Password, true);
                    hasErrors = true;
                }

                try
                {
                    _passwordChecker.Check(page.Password.Password);
                }
                catch (BaseCheckerException ex)
                {
                    errors.Append(ex.Message).Append("\n");
                    hasErrors = true;
                }

                if (errors.Length > 0 && hasErrors)
                {
                    ShowErrorMessage(errors.ToString());
                }
            }

            return hasErrors;
        }

        #endregion
    }
}
