﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    public class MainViewModel : BaseViewModel<MainViewModel>
    {
        #region Приватные поля

        /// <summary>
        /// Сервис для работы с пользовательской информацией, который связывается с сервером через SignalR Remote Procedure Call (RPC).
        /// Обеспечивает получение и отправку информации о пользователях, такую как их профили, настройки и список контактов.
        /// </summary>
        private readonly IUserService _userService;

        #endregion

        #region Свойства

        /// <summary>
        /// Свойство определяющее состояние загрузки текущей страницы
        /// </summary>
        public bool MainPageIsLoading
        {
            get => Get<bool>();
            set => Set(value);
        }
        
        /// <summary>
        /// Хранит экземпляр класса <see cref="ChatPage"/>
        /// </summary>
        public ChatPage? ChatPage
        {
            get => Get<ChatPage>();
            set => Set(value);
        }

        /// <summary>
        /// Хранит экземпляр класса <see cref="ProfilePage"/>
        /// </summary>
        public ProfilePage? ProfilePage
        {
            get => Get<ProfilePage>();
            set => Set(value);
        }

        #endregion

        #region Команды

        /// <summary>
        /// Команда, вызывающаяся при полной загрузке страницы
        /// </summary>
        public ICommand LoadedCommand { get; set; }

        /// <summary>
        /// Команда, использующая для перехода на страницу <see cref="LoginPage"/>
        /// </summary>
        public ICommand GoToLoginPageCommand { get; set; }

        /// <summary>
        /// Используется для выхода из аккаунта
        /// </summary>
        public ICommand ExitFromAccountCommand { get; set; }

        #endregion

        #region Конструкторы

        public MainViewModel()
        {
            this.LoadedCommand = new AsyncRelayParameterizedCommand(async (parameter) => await OnLoaded(parameter));
            this.GoToLoginPageCommand = new AsyncRelayCommand(async () => await GoToLoginPage());
            this._userService = IoC.Get<IUserService>();
            this.MainPageIsLoading = false;
            this.ExitFromAccountCommand = new AsyncRelayCommand(async () => await ExitFromAccount());
        }

        #endregion

        #region Методы

        /// <summary>
        /// Вызывается при полной загрузки страницы
        /// </summary>
        /// <param name="sender">Содержит саму страницу</param>
        public async Task OnLoaded(object? sender)
        {
            await RunCommand(() => MainPageIsLoading, async () =>
            {
                if (sender != null && (sender.GetType() == typeof(MainPage)))
                {
                    MainPage? mainPage = (MainPage)sender;

                    try
                    {
                        ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                        List<SectionRightDTO> sectionRights = await _userService.GetSectionRights(appVM?.CurrentUser?.Login);
                        SetTabItemVisibilityByUserRights(mainPage, sectionRights);
                    }
                    catch (ApplicationException exception)
                    {
                        ShowErrorMessage(exception.Message);
                        GoToLoginPageCommand.Execute(null);
                    }
                }
            });
        }

        /// <summary>
        /// Используется для перехода на логин страницу <see cref="LoginPage"/>
        /// </summary>
        public async Task GoToLoginPage()
        {
            await RunCommand(() => MainPageIsLoading, async () =>
            {
                IoC.Get<ApplicationViewModel>()
                   .GoToPage(await IAsyncInitializer<LoginPage>.CreateAsync());
            });
        }

        /// <summary>
        /// Используется для ассинхронной инициализации экземпляра текущего класса
        /// </summary>
        /// <returns>Экземпляр текущего класса</returns>
        public async override Task<MainViewModel> InitializeAsync()
        {
            this.ChatPage = await IAsyncInitializer<ChatPage>.CreateAsync();
            this.ProfilePage = await IAsyncInitializer<ProfilePage>.CreateAsync();
            return this;
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Используется для определения видимости для TabItem, основываясь на правах пользователя
        /// </summary>
        private void SetTabItemVisibilityByUserRights(MainPage mainPage, List<SectionRightDTO> sectionRights)
        {
            foreach (TabItem tabItem in mainPage.tabControl.Items)
            {
                SectionRightDTO? sectionRightDTO = sectionRights.Where(item => item?.SectionName?.ToLower() == tabItem.Name.ToLower()).FirstOrDefault();

                if (sectionRightDTO != null)
                {
                    tabItem.Visibility = sectionRightDTO.HasAccess ? Visibility.Visible : Visibility.Collapsed;
                } else
                {
                    tabItem.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// Используется для выхода из аккаунта
        /// </summary>
        /// <returns></returns>
        private async Task ExitFromAccount()
        {
            MessageBoxResult result = MessageBox.Show("Вы точно хотите выйти из аккаунта?", "Выход из аккаунта", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                await RunCommand(() => MainPageIsLoading, async () =>
                {
                    try
                    {
                        ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
                        UserHubConnection userHubConnection = IoC.Get<UserHubConnection>();
                        await userHubConnection.HubConnection.StopAsync();
                        await userHubConnection.HubConnection.StartAsync();
                        appVM.CurrentUser = null;
                        appVM.GoToPage(await IAsyncInitializer<LoginPage>.CreateAsync());
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Подключение к серверу не установлено", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                        Application.Current.Shutdown();
                    }
                });
            }
        }

        #endregion
    }
}
