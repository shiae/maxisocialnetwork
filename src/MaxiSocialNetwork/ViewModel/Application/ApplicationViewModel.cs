﻿using System;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Наследуется от <see cref="BaseViewModel"/>
    /// Является ViewModel целого приложения, это глобальный ViewModel
    /// </summary>
    public class ApplicationViewModel : BaseViewModel<ApplicationViewModel>
    {
        #region Публичные события

        /// <summary>
        /// Свойство используется для выполнения действия при изменении страницы
        /// </summary>
        public event EventHandler? CurrentPagePropertyChanged;

        #endregion

        #region Публичные методы

        /// <summary>
        /// Текущая отображаемая страница
        /// </summary>
        public BasePage? CurrentPage
        {
            get => Get<BasePage>();
            private set => Set(value);
        }

        /// <summary>
        /// Текущий авторизованный пользователь (если null - пользователь не авторизован)
        /// </summary>
        public UserDTO? CurrentUser
        {
            get => Get<UserDTO>();
            set => Set(value);
        }

        #endregion

        #region Публичные методы

        /// <summary>
        /// Метод, использующийся для переключения страниц
        /// </summary>
        /// <param name="page"></param>
        /// <param name="baseViewModel"></param>
        public void GoToPage(BasePage basePage)
        {
            CurrentPage = basePage;
            OnPropertyChanged();
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Используется для вызова события текущей страницы для указания, 
        /// что элементы пользовательского интерфейса поменялись
        /// </summary>
        private void OnPropertyChanged()
        {
            CurrentPagePropertyChanged?.Invoke(this, EventArgs.Empty);
        }

        #endregion
    }
}
