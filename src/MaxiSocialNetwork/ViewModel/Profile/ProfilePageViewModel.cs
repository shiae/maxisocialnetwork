﻿using MaxiSocialNetworkChecker;
using MaxiSocialNetworkChecker.Exceptions;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    public class ProfilePageViewModel : BaseViewModel<ProfilePageViewModel>
    {
        #region Поля

        /// <summary>
        /// Бизнес логика для юзера
        /// </summary>
        private readonly UserService _userService;

        /// <summary>
        /// Используется для проверки почты
        /// </summary>
        private readonly EmailChecker _emailChecker;

        /// <summary>
        /// Используется для проверки телефона
        /// </summary>
        private readonly PhoneChecker _phoneChecker;

        #endregion

        #region Команды

        /// <summary>
        /// ViewModel главного окна
        /// </summary>
        public ApplicationViewModel ApplicationViewModel
        {
            get => Get<ApplicationViewModel>()!;
            set => Set(value);
        }

        /// <summary>
        /// Текущий пользователь
        /// </summary>
        public UserDTO CurrentUser
        {
            get => Get<UserDTO>()!;
            set => Set(value);
        }

        /// <summary>
        /// Используется для выбора фотографии
        /// </summary>
        public ICommand SelectProfileImageCommand { get; set; }

        /// <summary>
        /// Используется для сохранения введенной информации
        /// </summary>
        public ICommand SaveInformationCommand { get; set; }

        /// <summary>
        /// Картинка профиля
        /// </summary>
        public byte[]? ProfileImage
        {
            get => Get<byte[]>();
            set
            {
                if (value == null)
                {
                    Set(File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", "Background", "default-profile-image.png")));
                }
                else
                {
                    Set(value);
                    ButtonSaveIsEnabled = true;
                    ProfileImageChanged = true;
                }
            }
        }

        /// <summary>
        /// True - если картинка профиля поменялась
        /// </summary>
        public bool ProfileImageChanged
        {
            get => Get<bool>()!;
            set => Set(value);
        }

        /// <summary>
        /// Почта
        /// </summary>
        public string? Email
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? DateOfBirth
        {
            get => Get<DateTime?>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string? Phone
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Ссылка на сайт
        /// </summary>
        public string? Website
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Название школы
        /// </summary>
        public string? School
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? FirstName
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string? LastName
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// Адрес пользователя
        /// </summary>
        public string? Address
        {
            get => Get<string>();
            set
            {
                Set(value);
                ButtonSaveIsEnabled = true;
            }
        }

        /// <summary>
        /// True - кнопка сохранить является доступной
        /// </summary>
        public bool ButtonSaveIsEnabled
        {
            get => Get<bool>();
            set => Set(value);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ProfilePageViewModel()
        {
            _userService = IoC.Get<UserService>();
            _emailChecker = IoC.Get<EmailChecker>();
            _phoneChecker = IoC.Get<PhoneChecker>();
            ApplicationViewModel = IoC.Get<ApplicationViewModel>();
            CurrentUser = ApplicationViewModel.CurrentUser!;
            FirstName = CurrentUser.FirstName;
            LastName = CurrentUser.LastName;
            Email = CurrentUser.Email;
            DateOfBirth = CurrentUser.DateOfBirth ?? DateTime.Today;
            Phone = CurrentUser.Phone;
            Website = CurrentUser.Website;
            School = CurrentUser.School;
            Description = CurrentUser.Description;
            Address = CurrentUser.Address;
            ProfileImage = CurrentUser.ProfileImage;
            SelectProfileImageCommand = new AsyncRelayCommand(async () => await SelectProfileImage());
            SaveInformationCommand = new AsyncRelayCommand(async () => await SaveInformation());
            ButtonSaveIsEnabled = false;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Используется для установки фотографии профиля
        /// </summary>
        /// <returns></returns>
        public async Task SelectProfileImage()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image Files (*.bmp;*.jpg;*.jpeg,*.png)|*.bmp;*.jpg;*.jpeg;*.png";

            if (openFileDialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);

                if (fileInfo.Length > 5 * 1024 * 1024)
                {
                    MessageBox.Show("Выбранный файл слишком большой. Максимальный размер 5 МБ.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    ProfileImage = await File.ReadAllBytesAsync(openFileDialog.FileName);
                }
            }
        }

        /// <summary>
        /// Используется для сохранения введенной информации
        /// </summary>
        public async Task SaveInformation()
        {
            if (!CheckErrors())
            {
                CurrentUser.FirstName = FirstName;
                CurrentUser.LastName = LastName;
                CurrentUser.Email = Email;
                CurrentUser.DateOfBirth = DateOfBirth;
                CurrentUser.Phone = Phone;
                CurrentUser.Website = Website;
                CurrentUser.School = School;
                CurrentUser.Description = Description;
                CurrentUser.Address = Address;

                if (ProfileImageChanged)
                {
                    CurrentUser.ProfileImage = ProfileImage;
                }

                ApplicationViewModel.CurrentUser = CurrentUser;
                ButtonSaveIsEnabled = false;

                await _userService.UpdateUser(CurrentUser);

                MessageBox.Show("Информация была успешно обновлена", "Успешно", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool CheckErrors()
        {
            StringBuilder errors = new StringBuilder();
            bool hasErrors = false;

            if (string.IsNullOrEmpty(FirstName))
            {
                hasErrors = true;
                errors.Append("Укажите имя!").Append("\n");
            }

            if (FirstName.Any(char.IsDigit))
            {
                hasErrors = true;
                errors.Append("В имени не могут быть цифры!").Append("\n");
            }

            if (string.IsNullOrEmpty(LastName))
            {
                hasErrors = true;
                errors.Append("Укажите фамилию!").Append("\n");
            }

            if (LastName.Any(char.IsDigit))
            {
                hasErrors = true;
                errors.Append("В фамилии не могут быть цифры!").Append("\n");
            }

            try
            {
                if (!string.IsNullOrEmpty(Email))
                {
                    _emailChecker.Check(Email);
                }
            }
            catch (BaseCheckerException ex)
            {
                errors.Append(ex.Message).Append("\n");
                hasErrors = true;
            }

            try
            {
                if (!string.IsNullOrEmpty(Phone))
                {
                    _phoneChecker.Check(Phone);
                }
            }
            catch (BaseCheckerException ex)
            {
                errors.Append(ex.Message).Append("\n");
                hasErrors = true;
            }

            if (errors.Length > 0 && hasErrors)
            {
                ShowErrorMessage(errors.ToString());
            }

            return hasErrors;
        }

        #endregion
    }
}
