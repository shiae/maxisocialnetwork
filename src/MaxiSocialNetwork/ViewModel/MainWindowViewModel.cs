﻿using System.Windows;
using System.Windows.Input;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// ViewModel для кастомного главного окна
    /// </summary>
    public class MainWindowViewModel : BaseViewModel<MainWindowViewModel>
    {
        #region Private Member

        /// <summary>
        /// Помощник по изменению размера окна, который поддерживает правильный размер окна в различных состояниях
        /// </summary>
        private WindowResizer mWindowResizer;

        /// <summary>
        /// Поле вокруг окна, чтобы создать отбрасываемую тень
        /// </summary>
        private Thickness mOuterMarginSize = new Thickness(5);

        /// <summary>
        /// Радиус краев окна
        /// </summary>
        private int mWindowRadius = 10;

        /// <summary>
        /// Последнее известное положение стыковки
        /// </summary>
        private WindowDockPosition mDockPosition = WindowDockPosition.Undocked;

        #endregion

        #region Public Properties

        /// <summary>
        /// Наименьшая ширина, до которой может доходить окно
        /// </summary>
        public double WindowMinimumWidth { get; set; } = 900;

        /// <summary>
        /// Наименьшая высота, на которую может подниматься окно
        /// </summary>
        public double WindowMinimumHeight { get; set; } = 500;

        /// <summary>
        /// True, если окно должно быть без полей, потому что оно закреплено или развернуто
        /// </summary>
        public bool Borderless => (Window.WindowState == WindowState.Maximized || mDockPosition != WindowDockPosition.Undocked);

        /// <summary>
        /// Размер границы изменения размера вокруг окна
        /// </summary>
        public int ResizeBorder => Window.WindowState == WindowState.Maximized ? 0 : 4;

        /// <summary>
        /// Размер границы изменения размера вокруг окна с учетом внешнего поля
        /// </summary>
        public Thickness ResizeBorderThickness => new Thickness(OuterMarginSize.Left + ResizeBorder,
                                                                OuterMarginSize.Top + ResizeBorder,
                                                                OuterMarginSize.Right + ResizeBorder,
                                                                OuterMarginSize.Bottom + ResizeBorder);

        /// <summary>
        /// Заполнение внутреннего содержимого главного окна
        /// </summary>
        public Thickness InnerContentPadding { get; set; } = new Thickness(0);

        /// <summary>
        /// Поле вокруг окна, чтобы создать отбрасываемую тень
        /// </summary>
        public Thickness OuterMarginSize
        {
            get => Window.WindowState == WindowState.Maximized ? mWindowResizer.CurrentMonitorMargin : (Borderless ? new Thickness(0) : mOuterMarginSize);
            set => mOuterMarginSize = value;
        }

        /// <summary>
        /// Радиус краев окна
        /// </summary>
        public int WindowRadius
        {
            get => Borderless ? 0 : mWindowRadius;
            set => mWindowRadius = value;
        }

        /// <summary>
        /// Прямоугольная граница вокруг окна при закреплении
        /// </summary>
        public int FlatBorderThickness => Borderless && Window.WindowState != WindowState.Maximized ? 1 : 0;

        /// <summary>
        /// Радиус краев окна
        /// </summary>
        public CornerRadius WindowCornerRadius => new CornerRadius(WindowRadius);

        /// <summary>
        /// Высота строки заголовка / подписи к окну
        /// </summary>
        public int TitleHeight { get; set; } = 42;

        /// <summary>
        /// Высота строки заголовка / подписи к окну
        /// </summary>
        public GridLength TitleHeightGridLength => new GridLength(TitleHeight + ResizeBorder);

        /// <summary>
        /// Верно, если у нас должно быть затемненное наложение на окно
        /// например, когда видно всплывающее окно или окно не сфокусировано
        /// </summary>
        public bool DimmableOverlayVisible { get; set; }

        /// <summary>
        /// Окно, которым управляет эта модель представления
        /// </summary>
        public Window Window { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Команда для сворачивания окна
        /// </summary>
        public ICommand MinimizeCommand { get; set; }

        /// <summary>
        /// Команда для разворачивания окна
        /// </summary>
        public ICommand MaximizeCommand { get; set; }

        /// <summary>
        /// Команда для закрытия окна
        /// </summary>
        public ICommand CloseCommand { get; set; }

        /// <summary>
        /// Команда для отображения системного меню окна
        /// </summary>
        public ICommand MenuCommand { get; set; }

        /// <summary>
        /// Текущая отображаемая страница
        /// </summary>
        public BasePage? CurrentPage => IoC.Get<ApplicationViewModel>().CurrentPage;

        /// <summary>
        /// Определяет конткект Maximazed кнопки
        /// </summary>
        public string MaximazedButtonContent
        {
            get
            {
                return Window.WindowState != WindowState.Maximized ? "◻" : "❐";
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Фабричный метод для создания экземпляра класса MainWindowViewModel
        /// </summary>
        /// <param name="window">Главный экран</param>
        /// <returns>Экзмпляр класса MainWindowViewModel</returns>
        public static MainWindowViewModel Create(Window window)
        {
            MainWindowViewModel mainWindowViewModel = new MainWindowViewModel();
            mainWindowViewModel.Window = window;
            mainWindowViewModel.Window.StateChanged += (sender, e) =>
            {
                mainWindowViewModel.WindowResized();
            };
            mainWindowViewModel.MinimizeCommand = new RelayCommand(() => mainWindowViewModel.Window.WindowState = WindowState.Minimized);
            mainWindowViewModel.MaximizeCommand = new RelayCommand(() => mainWindowViewModel.Window.WindowState ^= WindowState.Maximized);
            mainWindowViewModel.CloseCommand = new RelayCommand(() => mainWindowViewModel.Window.Close());
            mainWindowViewModel.MenuCommand = new RelayCommand(() => SystemCommands.ShowSystemMenu(mainWindowViewModel.Window, mainWindowViewModel.GetMousePosition()));
            mainWindowViewModel.mWindowResizer = new WindowResizer(mainWindowViewModel.Window);
            mainWindowViewModel.mWindowResizer.WindowDockChanged += (dock) =>
            {
                mainWindowViewModel.mDockPosition = dock;
                mainWindowViewModel.WindowResized();
            };
            mainWindowViewModel.mWindowResizer.WindowFinishedMove += () =>
            {
                if (mainWindowViewModel.mDockPosition == WindowDockPosition.Undocked && mainWindowViewModel.Window.Top == mainWindowViewModel.mWindowResizer.CurrentScreenSize.Top)
                {
                    mainWindowViewModel.Window.Top = -mainWindowViewModel.OuterMarginSize.Top;
                }
            };
            return mainWindowViewModel;
        }

        #endregion

        #region Private Helpers

        /// <summary>
        /// Возвращает текущее положение мыши на экране
        /// </summary>
        private Point GetMousePosition()
        {
            return mWindowResizer.GetCursorPosition();
        }

        /// <summary>
        /// Если размер окна изменяется в особое положение (закреплено или развернуто)
        /// это обновит все необходимые события изменения свойств для установки значений границ и радиуса
        /// </summary>
        private void WindowResized()
        {
            OnPropertyChanged(nameof(Borderless));
            OnPropertyChanged(nameof(FlatBorderThickness));
            OnPropertyChanged(nameof(ResizeBorderThickness));
            OnPropertyChanged(nameof(OuterMarginSize));
            OnPropertyChanged(nameof(WindowRadius));
            OnPropertyChanged(nameof(WindowCornerRadius));
            OnPropertyChanged(nameof(MaximazedButtonContent));
        }

        #endregion
    }
}
