﻿using System;
using System.Collections.Generic;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Модель для сообщения
    /// </summary>
    public class MessageDTO
    {
        /// <summary>
        /// ID сообщения
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Содержание сообщения
        /// </summary>
        public string? Text { get; set; }

        /// <summary>
        /// Дата отправки сообщения
        /// </summary>
        public DateTime? SentAt { get; set; }

        /// <summary>
        /// Дата обновления сообщения
        /// </summary>
        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// ID беседы сообщения
        /// </summary>
        public int ConversationId { get; set; }

        /// <summary>
        /// ID отправителя сообщения
        /// </summary>
        public int SenderId { get; set; }

        /// <summary>
        /// ID получатея сообщения
        /// </summary>
        public int? RecipientId { get; set; }

        /// <summary>
        /// Используется для хранения файлов к сообщению
        /// </summary>
        public List<FileDTO>? Files { get; set; }
    }
}
