﻿namespace MaxiSocialNetwork
{
    public class GetChatListItemByCurrentUserAndParticipateUserIdDTO
    {
        /// <summary>
        /// ID текущего пользователя
        /// </summary>
        public int CurrentUserId { get; set; }

        /// <summary>
        /// ID собеседника
        /// </summary>
        public int ParticipateUserId { get; set; }
    }
}
