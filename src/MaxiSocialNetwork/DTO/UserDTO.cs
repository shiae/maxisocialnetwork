﻿using System;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Класс, хранящий свойства пользователя
    /// </summary>
    public class UserDTO
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int? Id { get; set; }
        
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        public string? LastName { get; set; }
        
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string? Login { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string? Password { get; set; }

        /// <summary>
        /// Является ли пользователь в текущий момент онлайн
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// ID подключения
        /// </summary>
        public string? ConnectionId { get; set; }

        /// <summary>
        /// Фотография профиля
        /// </summary>
        public byte[]? ProfileImage { get; set; }

        /// <summary>
        /// Почта
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string? Phone { get; set; }

        /// <summary>
        /// Ссылка на сайт
        /// </summary>
        public string? Website { get; set; }

        /// <summary>
        /// Название школы
        /// </summary>
        public string? School { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Адрес пользователя
        /// </summary>
        public string? Address { get; set; }

        /// <summary>
        /// RGB цвет картинки пользователя
        /// </summary>
        public string? ProfileRgbImage { get; set; }

        /// <summary>
        /// True - если присутствует картинка
        /// </summary>
        public bool HasProfileImage { get; set; }

        /// <summary>
        /// Инициалы
        /// </summary>
        public string Initials { get; set; }
    }
}
