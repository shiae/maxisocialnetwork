﻿namespace MaxiSocialNetwork
{
    public class SectionRightDTO
    {
        public string? SectionName { get; set; }
        public bool HasAccess { get; set; }
    }
}
