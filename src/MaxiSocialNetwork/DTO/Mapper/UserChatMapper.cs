﻿using System;
using System.Collections.Generic;

namespace MaxiSocialNetwork
{
    public class UserChatMapper
    {
        /// <summary>
        /// Используется для конвертации <see cref="UserChatDTO"/> в <see cref="ChatListItemViewModel"/>
        /// </summary>
        /// <param name="userChatDTO"><see cref="UserChatDTO"/></param>
        /// <returns><see cref="ChatListItemViewModel"/></returns>
        public ChatListItemViewModel ToChatListItemViewModel(UserChatDTO userChatDTO)
        {
            return new ChatListItemViewModel()
            {
                MessageId = userChatDTO.MessageId,
                ConversationId = userChatDTO.ConversationId,
                FirstName = userChatDTO.FirstName,
                LastName = userChatDTO.LastName,
                UserId = userChatDTO.UserId,
                MessageDate = userChatDTO.MessageDate,
                HasUnreadMessages = userChatDTO.HasUnreadMessages,
                ProfileImage = userChatDTO.ProfileImage,
                ProfileRgbImage = userChatDTO.ProfileRgbImage,
                IsOnline = userChatDTO.IsOnline,
                SendSticker = string.IsNullOrEmpty(userChatDTO.Message),
                Message = DefineMessageOfChatListItemViewMode(userChatDTO.Message, userChatDTO.ConversationId)
            };
        }

        /// <summary>
        /// Используется для конвертации <see cref="UserDTO"/> в <see cref="ChatListItemViewModel"/>
        /// </summary>
        /// <param name="user"><see cref="UserDTO"/></param>
        /// <returns><see cref="ChatListItemViewModel"/></returns>
        public ChatListItemViewModel ToChatListItemViewModel(UserDTO user)
        {
            return new ChatListItemViewModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserId = (int)user.Id!,
                HasUnreadMessages = false,
                ProfileImage = user.ProfileImage,
                ProfileRgbImage = user.ProfileRgbImage,
                IsOnline = true,
                SendSticker = false,
                IsNewChat = true
            };
        }

        /// <summary>
        /// Используется для конвертации списка <see cref="UserChatDTO"/> в <see cref="ChatListItemViewModel"/>
        /// </summary>
        /// <param name="userChatDTO"></param>
        /// <returns></returns>
        public List<ChatListItemViewModel> ToChatListItemViewModel(List<UserChatDTO> userChats)
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            List<ChatListItemViewModel> chatListItemViewModels = new List<ChatListItemViewModel>();

            foreach (UserChatDTO userChat in userChats)
            {
                ChatListItemViewModel chatListItemViewModel = ToChatListItemViewModel(userChat);
                chatListItemViewModel.HasUnreadMessages = chatListItemViewModel.HasUnreadMessages && appVM?.CurrentUser?.Id == chatListItemViewModel.UserId;
                chatListItemViewModels.Add(chatListItemViewModel);
            }

            return chatListItemViewModels;
        }

        /// <summary>
        /// Используется для определения сообщения в списке чатов
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Сообщение для чата из списка</returns>
        private string? DefineMessageOfChatListItemViewMode(string? message, int? conversationId)
        {
            if (string.IsNullOrEmpty(message) && conversationId != null)
            {
                return "Отправлено медиа-сообщение";
            }
            else if (conversationId == null)
            {
                return string.Empty;
            }

            return message;
        }
    }
}
