﻿namespace MaxiSocialNetwork
{
    public class MessageMapper
    {
        /// <summary>
        /// Используется для конвертации <see cref="MessageDTO"/> в <see cref="ChatMessageItemViewModel"/>
        /// </summary>
        /// <param name="messageDTO">Сообщение</param>
        /// <returns><see cref="ChatMessageItemViewModel"/></returns>
        public ChatMessageItemViewModel ToChatMessageItemViewModel(MessageDTO messageDTO)
        {
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();

            return new ChatMessageItemViewModel()
            {
                Id = messageDTO.Id,
                SentAt = messageDTO.SentAt,
                UpdatedAt = messageDTO.UpdatedAt,
                Messsage = messageDTO.Text,
                SentByCurrentUser = (appVM?.CurrentUser?.Id == messageDTO.SenderId),
                Files = messageDTO?.Files
            };
        }

        /// <summary>
        /// Используется для конвертации в <see cref="EditMessageDTO"/>
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="conversationId">ID Беседы</param>
        /// <param name="flag">Показывает режим изменения(0 - изменяем, 1 - удаляем)</param>
        /// <returns><see cref="EditMessageDTO"/></returns>
        public EditMessageDTO ToEditMessageDTO(ChatMessageItemViewModel? message, int? conversationId, int flag = 0)
        {
            return new EditMessageDTO()
            {
                Message = message?.Messsage,
                ConversationId = conversationId,
                Id = message?.Id,
                Flag = flag
            };
        }
    }
}
