﻿namespace MaxiSocialNetwork
{
    public class FileDTO
    {
        /// <summary>
        /// Имя файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Расширение файла
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Данные файла
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Высота картинки
        /// </summary>
        public double Height { get; set; }

        /// <summary>
        /// Ширина картинки
        /// </summary>
        public double Width { get; set; }
    }
}
