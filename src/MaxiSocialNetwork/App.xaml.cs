﻿using Microsoft.AspNet.SignalR.Messaging;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Метод для настройки глобальных настроек приложения
        /// </summary>
        /// <param name="e"></param>
        protected override async void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Настраиваем главное окно приложения
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += OnUnobservedTaskException!;

            // Настраиваем и показываем MainWindow
            Current.MainWindow = new MainWindow();
            MainWindowViewModel mainWindowViewModel = MainWindowViewModel.Create(Current.MainWindow);
            Current.MainWindow.DataContext = mainWindowViewModel;
            Current.MainWindow.Show();

            // Настраиваем ApplicationViewModel
            ApplicationViewModel appVM = IoC.Get<ApplicationViewModel>();
            appVM.CurrentPagePropertyChanged += (s, e) => mainWindowViewModel.OnPropertyChanged(nameof(mainWindowViewModel.CurrentPage));

            // Создаем подключение к HubConnection
            UserHubConnection connection = IoC.Get<UserHubConnection>();
            
            try
            {
                await connection.HubConnection.StartAsync();
                connection.HubConnection.On<string>("ServerError", OnServerError);
                appVM.GoToPage(await IAsyncInitializer<LoginPage>.CreateAsync());
            }
            catch (Exception)
            {
                MessageBox.Show("Подключение к серверу не установлено", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowGlobalErrorMessage(((Exception)e.ExceptionObject).Message);
        }

        /// <summary>
        /// Используется в качестве глобального обработчика событий
        /// </summary>
        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        /// <summary>
        /// Используется когда происходит ошибка в асинхронном коде
        /// </summary>
        private void OnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            ShowGlobalErrorMessage(e.Exception.Message);
        }

        /// <summary>
        /// Используется когда происходит ошибка на сервере
        /// </summary>
        /// <param name="message">Сообщение</param>
        private void OnServerError(string message)
        {
            ShowGlobalErrorMessage(message);
        }

        private void ShowGlobalErrorMessage(string message)
        {
            MessageBox.Show($"Не волнуйтесь! Произошла непредвиденная ошибка приложения: {message}. Обратитесь к системному администратору!", "Непредвиденная ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
