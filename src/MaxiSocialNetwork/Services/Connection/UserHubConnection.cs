﻿using Microsoft.AspNetCore.SignalR.Client;
using System.Configuration;

namespace MaxiSocialNetwork
{
    public class UserHubConnection
    {
        #region Свойства

        /// <summary>
        /// HubConnection для выполнения RPC запросов на сервер
        /// </summary>
        public HubConnection HubConnection { get; private set; }

        /// <summary>
        /// Имя hub'a, на который мы будем делать запросы
        /// </summary>
        public string HubName { get; private set; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="hubName">Hub, на который будут выполняться основные запросы</param>
        public UserHubConnection()
        {
            HubName = "UserHub";
            HubConnection = new HubConnectionBuilder()
                .WithUrl($"{ConfigurationManager.AppSettings["BaseSignalRUrl"]}/{HubName}")
                .Build();
        }

        /// <summary>
        /// Деструктор, который останавливает подключение к HUB
        /// </summary>
        ~UserHubConnection()
        {
            HubConnection.StopAsync();
        }

        #endregion
    }
}
