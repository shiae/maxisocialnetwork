﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализация интерфейса <see cref="IMessageService"/>
    /// </summary>
    public class MessageService : BaseService<ResponseDTO>, IMessageService
    {
        #region Публичные константы

        public const string GET_ALL_BY_CONVERSATION_ID = "GetListOfMessagesByConversationId";
        public const string EDIT_MESSAGE = "EditMessage";
        public const string DELETE_MESSAGE = "DeleteMessage";

        #endregion

        #region Методы IMessageService

        /// <summary>
        /// Используется для получения списка сообщение по ID беседы
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Список сообщений</returns>
        public async Task<List<MessageDTO>> GetAllByConversationId(int conversationId)
        {
            ResponseDTO response = await InvokeAsync(GET_ALL_BY_CONVERSATION_ID, conversationId);
            return JsonConvert.DeserializeObject<List<MessageDTO>>(response?.Result?.ToString());
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="editMessageDTO">Новое сообщение</param>
        /// <returns>Измененное изменение</returns>
        public async Task EditMessage(EditMessageDTO editMessageDTO)
        {
            await InvokeAsyncVoid(EDIT_MESSAGE, editMessageDTO);
        }

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        public async Task DeleteMessage(int id)
        {
            await InvokeAsyncVoid(DELETE_MESSAGE, id);
        }

        #endregion
    }
}
