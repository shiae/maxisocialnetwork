﻿using Newtonsoft.Json;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализация интерфейса <see cref="IAuthenticationSerivce"/>
    /// </summary>
    public class AuthenticationService : BaseService<ResponseDTO>, IAuthenticationSerivce
    {
        #region Публичные константы

        public const string REGISTER_USER = "RegisterUser";
        public const string LOGIN_USER = "LoginUser";

        #endregion

        #region Публичные методы

        /// <summary>
        /// Метод используется для регистрации пользователя
        /// </summary>
        /// <param name="user">Пользовательские данные</param>
        /// <exception cref="AuthenticationException">Происходит при неуспешной регистрации</exception>
        public async Task RegisterUser(UserDTO user)
        {
            if (string.IsNullOrEmpty(user.Login) ||
                string.IsNullOrEmpty(user.Password) ||
                string.IsNullOrEmpty(user.FirstName) ||
                string.IsNullOrEmpty(user.LastName))
            {
                throw new ApplicationException(INCORRECT_PARAMETERS_ERROR_MSG);
            }

            await InvokeAsync(REGISTER_USER, user);
        }

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="user">Данные, введенные от пользователя</param>
        /// <exception cref="AuthenticationException">Происходит при неуспешной авторизации</exception>
        public async Task<UserDTO> LoginUser(UserDTO user)
        {
            if (string.IsNullOrEmpty(user.Login) ||
                string.IsNullOrEmpty(user.Password))
            {
                throw new ApplicationException(INCORRECT_PARAMETERS_ERROR_MSG);
            }

            ResponseDTO response = await InvokeAsync(LOGIN_USER, user);

            return JsonConvert.DeserializeObject<UserDTO>(response?.Result?.ToString());
        }

        #endregion
    }
}
