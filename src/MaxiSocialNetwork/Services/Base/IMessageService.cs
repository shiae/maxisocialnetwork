﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Интерфейс, предназначенный для реализации бизнес логики по работе с сообщениями
    /// </summary>
    public interface IMessageService
    {
        /// <summary>
        /// Используется для получения списка сообщение по ID беседы
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Список сообщений</returns>
        Task<List<MessageDTO>> GetAllByConversationId(int conversationId);

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="editMessageDTO">Новое сообщение</param>
        /// <returns>Измененное изменение</returns>
        Task EditMessage(EditMessageDTO editMessageDTO);

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        Task DeleteMessage(int id);
    }
}
