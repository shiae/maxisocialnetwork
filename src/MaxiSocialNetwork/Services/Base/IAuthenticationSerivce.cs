﻿using MaxiSocialNetwork;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Базовый интерфейс, предоставляющий методы для реализации аутентификации в приложении
    /// </summary>
    public interface IAuthenticationSerivce
    {
        /// <summary>
        /// Метод, использующийся для регистрации пользователя в базе данных
        /// </summary>
        /// <exception cref="AuthenticationException">происходит когда были переданы неправильным регистарционные данные</exception>
        /// <param name="userDTO">Данные, введенные от пользователя</param>
        Task RegisterUser(UserDTO userDTO);

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Данные, введенные от пользователя</param>
        /// <returns>Данные авторизованного пользователя</returns>
        Task<UserDTO> LoginUser(UserDTO userDTO);
    }
}
