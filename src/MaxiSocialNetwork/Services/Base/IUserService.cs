﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Интерфейс, предоставляющий методы для реализации бизнес логики, связанной с работой с пользователем
    /// </summary>
    public interface IUserService
    { 
        /// <summary>
        /// Метод используется для получения списка чатов
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список чатов</returns>
        Task<List<UserChatDTO>> GetChatList(string? login);

        /// <summary>
        /// Используется для получения прав на все секции по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав</returns>
        Task<List<SectionRightDTO>> GetSectionRights(string? login);

        /// <summary>
        /// Используется для отправки сообщения пользователю
        /// </summary>
        /// <param name="sendMessageDTO">Сообщение</param>
        Task SendMessage(SendMessageDTO sendMessageDTO);

        /// <summary>
        /// Используется для изменения статуса прочитанности сообщений беседы по ID
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="hasUnreadMessages">True - имеет непрочитанные сообщения</param>
        /// <returns>ID беседы</returns>
        Task EditHasUnreadMessageStatus(SetUnreadMessageDTO setUnreadMessageDTO);

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информция</param>
        /// <returns>ID пользователя измененного</returns>
        Task UpdateUser(UserDTO userDTO);

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        Task<UserDTO> GetUserById(int id);

        /// <summary>
        /// Используется для получения чата
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        Task<UserChatDTO?> GetChatListItemByCurrentUserAndParticipateUserId(GetChatListItemByCurrentUserAndParticipateUserIdDTO getChatListItemDTO);
    }
}
