﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Базовый класс для реализации Service паттерна
    /// </summary>
    /// <typeparam name="Params">Обьект, которые будет выступать в качестве ответа после запроса на сервер</typeparam>
    public abstract class BaseService<Response>
        where Response : ResponseDTO, new()
    {
        #region Глобальные константы

        public const string INCORRECT_PARAMETERS_ERROR_MSG = "Были переданые некорректные параметры";
        public const string RESULT_NOT_FOUND_ERROR_MSG = "Произошла ошибка получения результата от сервера";

        #endregion

        #region Свойства 

        /// <summary>
        /// Подключение к хабу 'UserHub'
        /// </summary>
        public UserHubConnection UserHubConnection => IoC.Get<UserHubConnection>();

        #endregion

        #region Protected методы

        /// <summary>
        /// Используется для асинхронного запроса на сервер по <see cref="hubName"/>
        /// </summary>
        /// <param name="params">Параметры</param>
        /// <returns>Результат запроса</returns>
        protected async Task<Response> InvokeAsync<Params>(string endpoint, Params @params)
        {
            Response? response;

            try
            {
                response = await UserHubConnection.HubConnection.InvokeCoreAsync<Response>(endpoint, new object[] { @params! });
            }
            catch (Exception ex)
            {
                throw new ServerException(ex.Message);
            }

            if (response == null)
            {
                throw new ServerException();
            }

            if (!response.IsSuccess)
            {
                if (response.TypeOfException == TypesOfExceptions.UNEXPECTED)
                {
                    throw new ServerException(response?.ErrorMessage);
                }
                else
                {
                    throw new ApplicationException(response?.ErrorMessage);
                }
            }

            return response;
        }

        /// <summary>
        /// Используется для асинхронного запроса на сервер по <see cref="hubName"/>
        /// </summary>
        /// <param name="params">Параметры</param>
        /// <returns>Результат запроса</returns>
        protected async Task InvokeAsyncVoid<Params>(string endpoint, Params @params)
        {
            await UserHubConnection.HubConnection.InvokeCoreAsync<Response>(endpoint, new object[] { @params! });
        }

        #endregion
    }
}
