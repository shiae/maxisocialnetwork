﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализация интерфейса <see cref="IUserService"/>
    /// </summary>
    public class UserService : BaseService<ResponseDTO>, IUserService
    {
        #region Публичный константы

        public const string GET_USER_CHAT_LIST_BY_LOGIN = "GetUserChatListByLogin";
        public const string GET_SECTION_RIGHTS_OF_USER_BY_LOGIN = "GetSectionRightsOfUserByLogin";
        public const string SEND_MESSAGE = "SendMessage";
        public const string EDIT_HAS_UNREAD_MESSAGE_STATUS = "EditHasUnreadMessageStatus";
        public const string UPDATE_USER = "UpdateUser";
        public const string GET_USER_BY_ID = "GetUserById";
        public const string GET_CHAT_LIST_ITEM_BY_CURRENT_USER_AND_PARTICIPATE_USER_ID = "GetChatListItemByCurrentUserAndParticipateUserId";

        #endregion

        #region Методы интерфейса IUserService

        /// <summary>
        /// Метод используется для получения списка чатов
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список чатов</returns>
        public async Task<List<UserChatDTO>> GetChatList(string? login)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ApplicationException(INCORRECT_PARAMETERS_ERROR_MSG);
            }

            ResponseDTO response = await InvokeAsync(GET_USER_CHAT_LIST_BY_LOGIN, login);

            return JsonConvert.DeserializeObject<List<UserChatDTO>>(response?.Result?.ToString());
        }

        /// <summary>
        /// Используется для получения прав на все секции по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав</returns>
        public async Task<List<SectionRightDTO>> GetSectionRights(string? login)
        {
            if (string.IsNullOrEmpty(login))
            {
                throw new ApplicationException(INCORRECT_PARAMETERS_ERROR_MSG);
            }

            ResponseDTO response = await InvokeAsync(GET_SECTION_RIGHTS_OF_USER_BY_LOGIN, login);

            return JsonConvert.DeserializeObject<List<SectionRightDTO>>(response?.Result?.ToString());
        }

        /// <summary>
        /// Используется для отправки сообщения пользователю
        /// </summary>
        /// <param name="sendMessageDTO">Сообщение</param>
        public async Task SendMessage(SendMessageDTO sendMessageDTO)
        {
            if (sendMessageDTO.RecipientId == 0 || 
                sendMessageDTO.SenderId == 0)
            {
                throw new ApplicationException(INCORRECT_PARAMETERS_ERROR_MSG);
            }

            await InvokeAsyncVoid(SEND_MESSAGE, sendMessageDTO);
        }

        /// <summary>
        /// Используется для изменения статуса прочитанности сообщений беседы по ID
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="hasUnreadMessages">True - имеет непрочитанные сообщения</param>
        /// <returns>ID беседы</returns>
        public async Task EditHasUnreadMessageStatus(SetUnreadMessageDTO setUnreadMessageDTO)
        {
            await InvokeAsyncVoid(EDIT_HAS_UNREAD_MESSAGE_STATUS, setUnreadMessageDTO);
        }

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информция</param>
        /// <returns>ID пользователя измененного</returns>
        public async Task UpdateUser(UserDTO userDTO)
        {
            await InvokeAsyncVoid(UPDATE_USER, userDTO);
        }

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        public async Task<UserDTO> GetUserById(int id)
        {
            ResponseDTO response = await InvokeAsync(GET_USER_BY_ID, id);
            return JsonConvert.DeserializeObject<UserDTO>(response?.Result?.ToString());
        }

        /// <summary>
        /// Используется для получения чата
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        public async Task<UserChatDTO?> GetChatListItemByCurrentUserAndParticipateUserId(GetChatListItemByCurrentUserAndParticipateUserIdDTO getChatListItemDTO)
        {
            ResponseDTO response = await InvokeAsync(GET_CHAT_LIST_ITEM_BY_CURRENT_USER_AND_PARTICIPATE_USER_ID, getChatListItemDTO);

            if (response.Result == null)
            {
                return null;
            }

            return JsonConvert.DeserializeObject<UserChatDTO>(response?.Result?.ToString());
        }

        #endregion
    }
}
