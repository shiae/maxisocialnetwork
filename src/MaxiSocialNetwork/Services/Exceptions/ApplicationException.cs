﻿using System;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Наследуется от класса <see cref="Exception"/>
    /// Используется при ошибках во время аутентификации
    /// </summary>
    public class ApplicationException : Exception
    {
        public ApplicationException(string? message = null) : base(message)
        {

        }
    }
}
