﻿using System;

namespace MaxiSocialNetwork
{
    public class ServerException : Exception
    {
        public ServerException(string? message = null) : base(message)
        {

        }
    }
}
