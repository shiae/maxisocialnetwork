﻿using System;
using System.Globalization;

namespace MaxiSocialNetwork
{
    public class ReverseBooleanConverter : BaseValueConverter<ReverseBooleanConverter>
    {
        public override object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !((bool)value);
        }
    }
}
