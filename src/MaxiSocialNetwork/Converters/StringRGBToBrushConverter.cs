﻿using System;
using System.Globalization;
using System.Windows.Media;

namespace MaxiSocialNetwork
{
    public class StringRGBToBrushConverter : BaseValueConverter<StringRGBToBrushConverter>
    {
        public override object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string? color = value == null ? "#d9544d" : value.ToString();
            return new BrushConverter().ConvertFrom(color) as SolidColorBrush; 
        }
    }
}
