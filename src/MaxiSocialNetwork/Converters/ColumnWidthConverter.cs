﻿using System;
using System.Globalization;

namespace MaxiSocialNetwork
{
    public class ColumnWidthConverter : BaseValueConverter<ColumnWidthConverter>
    {
        public override object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double width = (double)value;

            if (width == 200)
            {
                return 0;
            }
            else
            {
                return width;
            }
        }
    }
}
