﻿using System;
using System.Globalization;
using System.Windows;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Реализация абстрактного класса <see cref="BaseValueConverter"/>
    /// Используется для конвертации ширины колонки в <see cref="GridLength"/>
    /// </summary>
    public class ColumnMaxWidthConverter : BaseValueConverter<ColumnMaxWidthConverter>
    {
        /// <summary>
        /// Используется для конвертации ширины колонки в <see cref="GridLength"/>
        /// </summary>
        public override object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double minWidthOfChatList = Double.Parse(parameter.ToString());
            double columnWidth = (double)value;
            double newColumnWidth = columnWidth - minWidthOfChatList;

            if (newColumnWidth <= 0)
            {
                return columnWidth;
            }

            return newColumnWidth;
        }
    }
}
