﻿using System;
using System.Globalization;
using System.Windows;

namespace MaxiSocialNetwork
{
    /// <summary>
    /// Конвертирует <see cref="Boolean"/> в <see cref="Visibility"/>
    /// </summary>
    public class BooleanToVisiblityConverter : BaseValueConverter<BooleanToVisiblityConverter>
    {
        public override object? Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
