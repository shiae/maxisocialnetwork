﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IConversationRepository"/>
    /// </summary>
    public class ConversationRepository : IConversationRepository
    {
        #region Приватные поля

        /// <summary>
        /// Используется для получения списка сообщений по ID беседы
        /// </summary>
        private readonly ExistsConversationBySenderIdAndRecipientIdQuery _existsConversationBySenderIdAndRecipientIdQuery;

        /// <summary>
        /// Используется для создания беседы
        /// </summary>
        private readonly CreateConversationQuery _createConversationQuery;

        /// <summary>
        /// Используется для добавления участника в беседу
        /// </summary>
        private readonly AddParticipantToConversationQuery _addParticipantToConversationQuery;

        /// <summary>
        /// Используется для установки статуса, содержит ли беседа непрочитанные сообщения или нет
        /// </summary>
        private readonly SetUnreadMessagesStatusConversationQuery _setUnreadMessagesStatusConversationQuery;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ConversationRepository(ExistsConversationBySenderIdAndRecipientIdQuery existsConversationBySenderIdAndRecipientIdQuery,
                                      CreateConversationQuery createConversationQuery,
                                      AddParticipantToConversationQuery addParticipantToConversationQuery,
                                      SetUnreadMessagesStatusConversationQuery setUnreadMessagesStatusConversationQuery)
        {
            _existsConversationBySenderIdAndRecipientIdQuery = existsConversationBySenderIdAndRecipientIdQuery;
            _createConversationQuery = createConversationQuery;
            _addParticipantToConversationQuery = addParticipantToConversationQuery;
            _setUnreadMessagesStatusConversationQuery = setUnreadMessagesStatusConversationQuery;
        }

        #endregion

        #region Методы интерфейса IConversationRepository

        /// <summary>
        /// Определяет, существует ли беседа по ID отправителя и ID получателя
        /// </summary>
        /// <param name="senderId">ID отправителя</param>
        /// <param name="recipientId">ID получателя</param>
        /// <returns>True - существует</returns>
        public async Task<bool> ExistsBySenderIdAndRecipientId(int senderId, int recipientId)
        {
            return await _existsConversationBySenderIdAndRecipientIdQuery.Run(new ExistsConversationBySenderIdAndRecipientIdQuery.Params(senderId, recipientId));
        }

        /// <summary>
        /// Используется для создания новой беседы
        /// </summary>
        /// <param name="creatorId">Создатель</param>
        /// <returns>ID новой беседы</returns>
        public async Task<int> CreateConversation(int creatorId)
        {
            return await _createConversationQuery.Run(new CreateConversationQuery.Params(creatorId));
        }

        /// <summary>
        /// Используется для добавления нового участника в беседу
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>ID новоего участника</returns>
        public async Task<int> AddParticipant(int userId, int conversationId)
        {
            return await _addParticipantToConversationQuery.Run(new AddParticipantToConversationQuery.Params(userId, conversationId));
        }

        /// <summary>
        /// Используется для изменения статуса прочитанности сообщений беседы по ID
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="hasUnreadMessages">True - имеет непрочитанные сообщения</param>
        /// <returns>ID беседы</returns>
        public async Task<int> EditHasUnreadMessageStatus(int conversationId, bool hasUnreadMessages)
        {
            return await _setUnreadMessagesStatusConversationQuery.Run(new SetUnreadMessagesStatusConversationQuery.Params(conversationId, hasUnreadMessages));
        }

        #endregion
    }
}
