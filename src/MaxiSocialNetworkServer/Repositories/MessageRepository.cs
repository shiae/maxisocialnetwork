﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IMessageRepository"/>
    /// </summary>
    public class MessageRepository : IMessageRepository
    {
        #region Публичные константы

        public const string SAVE_MESSAGE_ERROR_MSG = "Не возможно сохранить сообщение в несуществующую беседу!";

        #endregion

        #region Приватные поля

        /// <summary>
        /// Используется для получения списка сообщений по ID беседы
        /// </summary>
        private readonly GetListOfMessagesByConversationIdQuery _getListOfMessagesByConversationIdQuery;

        /// <summary>
        /// Используется для сохранения сообщения
        /// </summary>
        private readonly SaveMessageQuery _saveMessageQuery;

        /// <summary>
        /// Используется для получения сообщения по ID
        /// </summary>
        private readonly GetMessageByIdQuery _getMessageByIdQuery;

        /// <summary>
        /// Используется для сохранения файла в БД
        /// </summary>
        private readonly SaveFileQuery _saveFileQuery;

        /// <summary>
        /// Используеться для изменения текста
        /// </summary>
        private readonly EditMessageQuery _editMessageQuery;

        /// <summary>
        /// Используется для удаления сообщения по ID
        /// </summary>
        private readonly DeleteMessageByIdQuery _deleteMessageByIdQuery;

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MessageRepository(GetListOfMessagesByConversationIdQuery getListOfMessagesByConversationIdQuery, 
                                 SaveMessageQuery saveMessageQuery,
                                 GetMessageByIdQuery getMessageByIdQuery,
                                 SaveFileQuery saveFileQuery, 
                                 EditMessageQuery editMessageQuery,
                                 DeleteMessageByIdQuery deleteMessageByIdQuery)
        {
            _getListOfMessagesByConversationIdQuery = getListOfMessagesByConversationIdQuery;
            _saveMessageQuery = saveMessageQuery;
            _getMessageByIdQuery = getMessageByIdQuery;
            _saveFileQuery = saveFileQuery;
            _editMessageQuery = editMessageQuery;
            _deleteMessageByIdQuery = deleteMessageByIdQuery;
        }

        #endregion

        #region Методы IMessageRepository

        /// <summary>
        /// Используется для получения списка сообщение по ID беседы
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Список сообщений</returns>
        public async Task<List<MessageDTO>> GetAllByConversationId(int conversationId)
        {
            return await _getListOfMessagesByConversationIdQuery.Run(new GetListOfMessagesByConversationIdQuery.Params(conversationId));
        }

        /// <summary>
        /// Используется для сохранения сообщения в базу данных
        /// </summary>
        /// <param name="message">Сообщение</param>
        public async Task<int> SaveMessage(MessageDTO message)
        {
            if (message.ConversationId == null)
            {
                throw new ServerException(SAVE_MESSAGE_ERROR_MSG);
            }

            int id = await _saveMessageQuery.Run(new SaveMessageQuery.Params(message.Text, (int) message.ConversationId, message.SenderId));

            if (message.Files != null && message.Files.Count > 0)
            {
                foreach (var file in message.Files)
                {
                    await _saveFileQuery.Run(new SaveFileQuery.Params(file.Data, file.FileName, file.Extension, file.Height, file.Width, id));
                }
            }

            return id;
        }

        /// <summary>
        /// Используется для получения сообщения по ID
        /// </summary>
        /// <param name="id">ID сообщения</param>
        /// <returns>Сообщение</returns>
        public async Task<MessageDTO?> GetMessageById(int id)
        {
            return await _getMessageByIdQuery.Run(new GetMessageByIdQuery.Params(id));
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="editMessageDTO">Новое сообщение</param>
        /// <returns>Измененное изменение</returns>
        public async Task<MessageDTO?> EditMessage(EditMessageDTO editMessageDTO)
        {
            await _editMessageQuery.Run(new EditMessageQuery.Params((int)editMessageDTO.Id!, editMessageDTO.Message!));
            return await GetMessageById((int)editMessageDTO.Id!);
        }

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        public async Task DeleteMessageById(int id)
        {
            await _deleteMessageByIdQuery.Run(new DeleteMessageByIdQuery.Params(id));
        }

        #endregion
    }
}
