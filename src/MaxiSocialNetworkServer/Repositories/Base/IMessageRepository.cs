﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Интерфейс, предназначенный для реализации репозитория по работе с сообщениями
    /// </summary>
    public interface IMessageRepository
    {
        /// <summary>
        /// Используется для получения списка сообщение по ID беседы
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Список сообщений</returns>
        Task<List<MessageDTO>> GetAllByConversationId(int conversationId);

        /// <summary>
        /// Используется для сохранения сообщения в базу данных
        /// </summary>
        /// <param name="message">Сообщение</param>
        Task<int> SaveMessage(MessageDTO message);

        /// <summary>
        /// Используется для получения сообщения по ID
        /// </summary>
        /// <param name="id">ID сообщения</param>
        /// <returns>Сообщение</returns>
        Task<MessageDTO?> GetMessageById(int id);

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="editMessageDTO">Новое сообщение</param>
        /// <returns>Измененное изменение</returns>
        Task<MessageDTO?> EditMessage(EditMessageDTO editMessageDTO);

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        Task DeleteMessageById(int id);
    }
}
