﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Интерфейс, предназначенный для реализации репозитория по работе с беседами
    /// </summary>
    public interface IConversationRepository
    {
        /// <summary>
        /// Определяет, существует ли беседа по ID отправителя и ID получателя
        /// </summary>
        /// <param name="senderId">ID отправителя</param>
        /// <param name="recipientId">ID получателя</param>
        /// <returns>True - существует</returns>
        Task<bool> ExistsBySenderIdAndRecipientId(int senderId, int recipientId);

        /// <summary>
        /// Используется для создания новой беседы
        /// </summary>
        /// <param name="creatorId">Создатель</param>
        /// <returns>ID новой беседы</returns>
        Task<int> CreateConversation(int creatorId);

        /// <summary>
        /// Используется для добавления нового участника в беседу
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>ID новоего участника</returns>
        Task<int> AddParticipant(int userId, int conversationId);

        /// <summary>
        /// Используется для изменения статуса прочитанности сообщений беседы по ID
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="hasUnreadMessages">True - имеет непрочитанные сообщения</param>
        /// <returns>ID беседы</returns>
        Task<int> EditHasUnreadMessageStatus(int conversationId, bool hasUnreadMessages);
    }
}
