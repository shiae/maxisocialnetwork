﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IUserRepository"/>
    /// </summary>
    public class UserRepository : IUserRepository
    {
        #region Приватные поля

        /// <summary>
        /// Используется для проверки существования пользователя по его логину
        /// </summary>
        private readonly ExistsUserByLoginQuery _existsUserByLoginQuery;

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        private readonly SaveUserQuery _saveUserQuery;

        /// <summary>
        /// Используется для получения пользователя по логину и паролю
        /// </summary>
        private readonly GetUserByLoginAndPasswordQuery _getUserByLoginAndPasswordQuery;

        /// <summary>
        /// Используется для получения списка чатов конкретного пользователя
        /// </summary>
        private readonly GetChatListOfUserByLoginQuery _getChatListOfUserByLoginQuery;

        /// <summary>
        /// Используется для получения списка прав на секции конкретного пользователя по логину
        /// </summary>
        private readonly GetSectionRightsOfUserByLoginQuery _getSectionRightsOfUserQuery;

        /// <summary>
        /// Используется для установки онлайн статуса пользователю
        /// </summary>
        private readonly SetOnlineStatusQuery _setOnlineStatusQuery;

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        private readonly UpdateUserQuery _updateUserQuery;

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        private readonly GetUserByIdQuery _getUserByIdQuery;

        /// <summary>
        /// Используется для получения чата
        /// </summary>
        private readonly GetChatListItemByUserLoginAndRecipientIdQuery _getChatListItemByUserLoginAndRecipient;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public UserRepository(ExistsUserByLoginQuery existsUserByLoginQuery,
                              SaveUserQuery saveUserQuery,
                              GetUserByLoginAndPasswordQuery getUserByLoginAndPasswordQuery,
                              GetChatListOfUserByLoginQuery getChatListOfUserByLoginQuery,
                              GetSectionRightsOfUserByLoginQuery getSectionRightsOfUserQuery,
                              SetOnlineStatusQuery setOnlineStatusQuery,
                              UpdateUserQuery updateUserQuery,
                              GetUserByIdQuery getUserByIdQuery,
                              GetChatListItemByUserLoginAndRecipientIdQuery getChatListItemByUserLoginAndRecipient)
        {
            _existsUserByLoginQuery = existsUserByLoginQuery;
            _saveUserQuery = saveUserQuery;
            _getUserByLoginAndPasswordQuery = getUserByLoginAndPasswordQuery;
            _getChatListOfUserByLoginQuery = getChatListOfUserByLoginQuery;
            _getSectionRightsOfUserQuery = getSectionRightsOfUserQuery;
            _setOnlineStatusQuery = setOnlineStatusQuery;
            _updateUserQuery = updateUserQuery;
            _getUserByIdQuery = getUserByIdQuery;
            _getChatListItemByUserLoginAndRecipient = getChatListItemByUserLoginAndRecipient;
        }

        #endregion

        #region Методы интерфейса IUserRepository

        /// <summary>
        /// Используется для проверки существования пользователя по логину в базе данных
        /// </summary>
        /// <param name="userDTO">Хранит логин</param>
        /// <returns>True - если существует по логину</returns>
        public Task<bool> ExistsUserByLogin(UserDTO userDTO)
        {
            return _existsUserByLoginQuery.Run(new ExistsUserByLoginQuery.Params(userDTO.Login));
        }

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        /// <param name="userDTO">Пользователь</param>
        /// <returns>Количество затронутых строк</returns>
        public Task<int> SaveUser(UserDTO userDTO)
        {
            return _saveUserQuery.Run(new SaveUserQuery.Params(userDTO));
        }

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Основная информация пользователя для авторизации</param>
        /// <returns>Авторизованного пользователя</returns>
        public async Task<UserDTO?> GetUserByLoginAndPassword(UserDTO userDTO)
        {
            return await _getUserByLoginAndPasswordQuery.Run(new GetUserByLoginAndPasswordQuery.Params(userDTO.Login, userDTO.Password));
        }

        /// <summary>
        /// Метод используется для получения списка чатов
        /// </summary>
        /// <param name="userDTO">Пользователь, список чатов которого нужно отдать</param>
        /// <returns>Список чатов</returns>
        public async Task<List<UserChatDTO>> GetChatList(string login)
        {
            return await _getChatListOfUserByLoginQuery.Run(new GetChatListOfUserByLoginQuery.Params(login));
        }

        /// <summary>
        /// Используется для получения прав на все секции по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав</returns>
        public async Task<List<SectionRightDTO>> GetSectionRights(string login)
        {
            return await _getSectionRightsOfUserQuery.Run(new GetSectionRightsOfUserByLoginQuery.Params(login));
        }

        /// <summary>
        /// Используется для установки онлайн статуса пользователю
        /// </summary>
        /// <param name="isOnline">True - если пользователь онлайн</param>
        /// <param name="id">ID пользователя</param>
        public async Task SetOnlineStatus(int id, bool isOnline)
        {
            await _setOnlineStatusQuery.Run(new SetOnlineStatusQuery.Params(id, isOnline));
        }

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информция</param>
        /// <returns>ID пользователя измененного</returns>
        public async Task<int> UpdateUser(UserDTO userDTO)
        {
            return await _updateUserQuery.Run(new UpdateUserQuery.Params(
                    (int)userDTO.Id!,
                    userDTO.FirstName,
                    userDTO.LastName,
                    userDTO.ProfileImage,
                    userDTO.Email,
                    userDTO.DateOfBirth,
                    userDTO.Phone,
                    userDTO.Website,
                    userDTO.School,
                    userDTO.Description,
                    userDTO.Address
                ));
        }


        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        public async Task<UserDTO> GetUserById(int id)
        {
            return await _getUserByIdQuery.Run(new GetUserByIdQuery.Params(id));
        }

        /// <summary>
        /// Используется для получения элемента в чате по ID текущего пользователя и ID собеседника
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        public async Task<UserChatDTO?> GetChatListItemByCurrentUserAndParticipateUserId(int currentUserId, int participateUserId)
        {
            return await _getChatListItemByUserLoginAndRecipient.Run(new GetChatListItemByUserLoginAndRecipientIdQuery.Params(currentUserId, participateUserId));
        }

        #endregion
    }
}
