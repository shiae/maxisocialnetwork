﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class DeleteMessageByIdQuery : Query<int, DeleteMessageByIdQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        public override string Sql => @"
            DELETE FROM Files WHERE MessageId = @Id;
            DELETE FROM Messages WHERE Id = @Id;
        ";

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public DeleteMessageByIdQuery(SqlDatabase sqlDataBase) : base(sqlDataBase)
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Используется для выполнения запроса в базе данных
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override async Task<int> Run(Params @params)
        {
            return await database.Execute(Sql, ConvertToSqlParameters(@params));
        }

        /// <summary>
        /// Используется для получения сообщения
        /// </summary>
        /// <returns>Новое сообщение</returns>
        protected override Task<int> Map(SqlDataReader reader)
        {
            throw new NotSupportedException();
        }

        #endregion

        public class Params
        {
            public int Id { get; set; }

            public Params(int id)
            {
                Id = id;
            }
        }
    }
}
