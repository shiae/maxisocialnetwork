﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SetIsOnlineStatusForAllUsersQuery : Query<int, SetIsOnlineStatusForAllUsersQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        public override string Sql => @"
            UPDATE Users SET IsOnline = @IsOnline;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public SetIsOnlineStatusForAllUsersQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Устанавливает статус в сети для всех пользователей в системе
        /// </summary>
        /// <param name="params">Пользователь</param>
        /// <returns>Кол</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override async Task<int> Run(Params @params)
        {
            return await database.Execute(Sql, ConvertToSqlParameters(@params));
        }

        /// <summary>
        /// Не поддерживается в текущей реализации
        /// </summary>
        /// <exception cref="NotImplementedException">В случае вызова</exception>
        protected override Task<int> Map(SqlDataReader reader)
        {
            throw new NotSupportedException();
        }

        #endregion

        /// <summary>
        /// Хранит в себе пользователя, который будет сохранен в БД
        /// </summary>
        public class Params
        {
            public bool IsOnline { get; set; }

            public Params(bool isOnline)
            {
                IsOnline = isOnline;
            }
        }
    }
}
