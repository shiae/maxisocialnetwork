﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class AddParticipantToConversationQuery : Query<int, AddParticipantToConversationQuery.Params>
    {
        #region Свойства Query 

        /// <summary>
        /// Используется для добавления участника в беседу
        /// </summary>
        public override string Sql => @"
            INSERT INTO Participants(ConversationId, UserId, CreatedAt)
            VALUES (@ConversationId, @UserId, GETDATE());

            SELECT CAST(scope_identity() AS int) AS ParticipantId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public AddParticipantToConversationQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса CreateConversationQuery

        /// <summary>
        /// Используется для маппинга ID нового участника
        /// </summary>
        /// <param name="reader">Курсор</param>
        protected async override Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("ParticipantId"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public int UserId { get; set; }
            public int ConversationId { get; set; }

            public Params(int userId, int conversationId)
            {
                UserId = userId;
                ConversationId = conversationId;
            }
        }
    }
}
