﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class GetSectionRightsOfUserByLoginQuery : Query<List<SectionRightDTO>, GetSectionRightsOfUserByLoginQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для получения всех прав на секции пользователя по логину
        /// </summary>
        public override string Sql => @"
            SELECT
				s.Constant AS SectionName,
				sr.HasAccess
            FROM
				Users u
            INNER JOIN Roles r
				ON u.RoleId = r.Id
            INNER JOIN SectionsRights sr
				ON r.Id = sr.RoleId
            INNER JOIN Sections s
				ON sr.SectionId = s.Id
            WHERE
				u.Login = @Login;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public GetSectionRightsOfUserByLoginQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для маппинга данных из SQL в сущность <see cref="SectionRightDTO"/>
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>Список <see cref="SectionRightDTO"/></returns>
        protected override async Task<List<SectionRightDTO>> Map(SqlDataReader reader)
        {
            List<SectionRightDTO> result = new List<SectionRightDTO>();

            while (await reader.ReadAsync())
            {
                SectionRightDTO sectionRightDTO = new SectionRightDTO()
                {
                    SectionName = reader.GetString(reader.GetOrdinal("SectionName")),
                    HasAccess = reader.GetBoolean(reader.GetOrdinal("HasAccess"))
                };
                result.Add(sectionRightDTO);
            }

            return result;
        }

        #endregion

        public class Params
        {
            public string Login { get; set; }

            public Params(string login)
            {
                Login = login;
            }
        }
    }
}
