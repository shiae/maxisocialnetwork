﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SaveMessageQuery : Query<int, SaveMessageQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для сохранения сообщения в базу данных
        /// </summary>
        public override string Sql => @"
            INSERT INTO Messages(Text, ConversationId, SentAt, UpdatedAt, SenderId)
            VALUES (@Text, @ConversationId, GETDATE(), GETDATE(), @SenderId);

            SELECT CAST(scope_identity() AS int) AS MessageId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public SaveMessageQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для получения ID сообщения
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>Сущность <see cref="UserDTO"/></returns>
        protected override async Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("MessageId"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public string? Text { get; set; }
            public int ConversationId { get; set; }
            public int SenderId { get; set; }

            public Params(string? text, int conversationId, int senderId)
            {
                Text = text;
                ConversationId = conversationId;
                SenderId = senderId;
            }
        }
    }
}
