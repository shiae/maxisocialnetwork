﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Класс для непосредственной работы с базой данных
    /// </summary>
    public class SqlDatabase
    {
        #region Приватные поля

        /// <summary>
        /// Поле, хранящее в себе строку подключения к базе данных
        /// </summary>
        private readonly string? connectionString;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="connectionString">Строка подключения</param>
        public SqlDatabase(string? connectionString)
        {
            this.connectionString = connectionString;
        }

        #endregion

        #region Методы класса SqlDatabase

        /// <summary>
        /// Используется для создания подключения с БД
        /// </summary>
        /// <returns></returns>
        protected async Task<SqlConnection> CreateConnection()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            await connection.OpenAsync();
            return connection;
        }

        /// <summary>
        /// Используется для выполнения запроса в БД с возвращением какого - либо значения
        /// </summary>
        /// <typeparam name="T">Возращаемое значение</typeparam>
        /// <param name="query">Запрос, который необходимо выполнить</param>
        /// <param name="callback">Функция, которая используется для конвертации данных в сущности</param>
        /// <param name="parameters">Параметры для запроса</param>
        /// <returns>Результат выполнения запроса</returns>
        public async Task<T> Execute<T>(string query, Func<SqlDataReader, Task<T>> callback, params SqlParameter[] parameters)
        {
            using (SqlConnection connection = await CreateConnection())
            using (SqlCommand command = connection.CreateCommand())
            {   
                command.CommandText = query;
                command.Parameters.AddRange(parameters);

                using (SqlDataReader reader = await command.ExecuteReaderAsync())
                {
                    return await callback(reader);
                }
            }
        }

        /// <summary>
        /// Используется для выполнения запросов, не возвращающих данные
        /// </summary>
        /// <param name="query">Запрос, который необходимо выполнить</param>
        /// <param name="parameters">Параметры для запроса</param>
        public async Task<int> Execute(string query, params SqlParameter[] parameters)
        {
            using (SqlConnection connection = await CreateConnection())
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = query;
                command.Parameters.AddRange(parameters);

                return await command.ExecuteNonQueryAsync();
            }
        }

        #endregion
    }
}
