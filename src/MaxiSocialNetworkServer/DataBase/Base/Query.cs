﻿using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Базовый класс для реализации непосредственного запроса для выполнения в базе данных
    /// </summary>
    /// <typeparam name="T">Возвращаемое значение после выполнения</typeparam>
    public abstract class Query<T, Params>
        where Params : class
    {
        #region Protected поля

        /// <summary>
        /// Используемая база данных
        /// </summary>
        protected readonly SqlDatabase database;

        #endregion

        #region Абстрактные свойства

        /// <summary>
        /// Хранит в себе SQL запрос
        /// </summary>
        public abstract string Sql { get; }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">Используемая база данных</param>
        public Query(SqlDatabase database)
        {
            this.database = database;
        }

        #endregion

        #region Методы

        /// <summary>
        /// Используется для конвертации <see cref="Params"/> в <see cref="SqlParameter[]"/>
        /// </summary>
        /// <param name="params">Параметры, которые будут конвертироваться</param>
        /// <returns>Итоговый результат представленный в виде массива параметров <see cref="SqlParameter[]"/></returns>
        protected SqlParameter[] ConvertToSqlParameters(Params @params)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            Type paramsType = typeof(Params);

            foreach (PropertyInfo property in paramsType.GetProperties())
            {
                object? value = property.GetValue(@params);
                SqlParameter sqlParameter;

                if (property.PropertyType == typeof(byte[]) && value == null)
                {
                    sqlParameter = new SqlParameter(property.Name, SqlDbType.VarBinary, -1);
                    sqlParameter.Value = value ?? DBNull.Value;
                }
                else if (value == null)
                {
                    sqlParameter = new SqlParameter(property.Name, DBNull.Value);
                }
                else
                {
                    sqlParameter = new SqlParameter(property.Name, value);
                }

                sqlParameters.Add(sqlParameter);
            }

            return sqlParameters.ToArray();
        }

        /// <summary>
        /// Используется для выполнения запроса в базе данных
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public virtual async Task<T> Run(Params @params)
        {
            return await database.Execute(Sql, Map, ConvertToSqlParameters(@params));
        }

        #endregion

        #region Абстрактные методы

        /// <summary>
        /// Используется для выполнения конвертирования данных из запроса в сущности
        /// </summary>
        /// <param name="reader">Хранит в себе результат выполнения SQL запроса</param>
        /// <returns>Результат конвертации изи <see cref="SqlDataReader"/></returns>
        protected abstract Task<T> Map(SqlDataReader reader);

        #endregion
    }
}
