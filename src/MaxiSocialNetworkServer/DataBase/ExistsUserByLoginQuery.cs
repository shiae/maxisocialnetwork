﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация абстрактного класса <see cref="Query"/>
    /// Используется для проверки существования пользователя в базе данных по его логину
    /// </summary>
    public class ExistsUserByLoginQuery : Query<bool, ExistsUserByLoginQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для проверки существования пользователя по его Логину
        /// </summary>
        public override string Sql => @"
            SELECT
                COALESCE(MAX(1), 0) AS IsExists
            FROM
                Users u
            WHERE
                u.Login = @Login;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public ExistsUserByLoginQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для маппинга результата выполнения запроса в bool
        /// </summary>
        /// <returns>True - если существует</returns>
        protected override async Task<bool> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("IsExists")) == 1;
            }

            return false;
        }

        #endregion

        /// <summary>
        /// Хранит в себе Логин пользователя
        /// </summary>
        public class Params
        {
            public string? Login { get; set; }

            public Params(string? login)
            {
                Login = login;
            }
        }
    }
}
