﻿using Microsoft.EntityFrameworkCore.SqlServer.Scaffolding.Internal;
using System.Data;
using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class GetMessageByIdQuery : Query<MessageDTO?, GetMessageByIdQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для получения сообщения по его ID
        /// </summary>
        public override string Sql => @"
            SELECT
                m.Id,
                m.Text,
                m.ConversationId,
                m.SentAt,
                m.UpdatedAt,
                m.SenderId,
                p.UserId AS RecipientId,
                f.Id AS FileId,
                f.FileData,
                f.FileName,
                f.Extension,
                f.Height,
                f.Width
            FROM
                Messages m
            INNER JOIN Conversations c
                ON m.ConversationId = c.Id
            INNER JOIN Participants p
                ON c.Id = p.ConversationId
            LEFT JOIN Files f
                on m.Id = f.MessageId
            WHERE
                m.Id = @Id
                AND p.UserId != m.SenderId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public GetMessageByIdQuery(SqlDatabase sqlDatabase) : base(sqlDatabase) 
        { 

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Используется для маппинга результата в <see cref="MessageDTO"/>
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>Сообщение <see cref="MessageDTO"/></returns>
        protected override async Task<MessageDTO?> Map(SqlDataReader reader)
        {
            MessageDTO? messageDTO = null;

            while (await reader.ReadAsync())
            {
                if (messageDTO == null)
                {
                    messageDTO = new MessageDTO()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Text = reader["Text"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Text")),
                        ConversationId = reader.GetInt32(reader.GetOrdinal("ConversationId")),
                        SentAt = reader.GetDateTime(reader.GetOrdinal("SentAt")),
                        UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt")),
                        SenderId = reader.GetInt32(reader.GetOrdinal("SenderId")),
                        RecipientId = reader.GetInt32(reader.GetOrdinal("RecipientId")),
                        Files = new List<FileDTO>()
                    };
                }

                if (reader["FileId"] is not DBNull)
                {
                    FileDTO fileDTO = new FileDTO
                    {
                        Data = (byte[])reader.GetValue("FileData"),
                        FileName = reader.GetString(reader.GetOrdinal("FileName")),
                        Extension = reader.GetString(reader.GetOrdinal("Extension")),
                        Height = reader.GetDouble(reader.GetOrdinal("Height")),
                        Width = reader.GetDouble(reader.GetOrdinal("Width"))
                    };

                    messageDTO?.Files?.Add(fileDTO);
                }
            }

            return messageDTO;
        }

        #endregion

        public class Params
        {
            public int Id { get; set; }

            public Params(int id)
            {
                Id = id;
            }
        }
    }
}
