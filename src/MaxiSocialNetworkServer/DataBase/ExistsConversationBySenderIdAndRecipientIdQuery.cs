﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class ExistsConversationBySenderIdAndRecipientIdQuery : Query<bool, ExistsConversationBySenderIdAndRecipientIdQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для определения существования беседы по SenderId и RecipientId
        /// </summary>
        public override string Sql => @"
            SELECT
                COALESCE(MAX(1), 0)
            FROM
                (SELECT
                     t.ConversationId
                 FROM
                     (SELECT
                          p.ConversationId
                      FROM
                          Participants p
                      GROUP BY
                          ConversationId
                      HAVING
                          COUNT(*) = 2) t
                 WHERE
                     EXISTS (SELECT
                                 NULL
                             FROM
                                 Participants p
                             WHERE
                                 t.ConversationId = p.ConversationId
                                 AND p.UserId = @SenderId)) convs
            WHERE
                EXISTS (SELECT
                            NULL
                        FROM
                            Participants p
                        WHERE
                            convs.ConversationId = p.ConversationId
                            AND p.UserId = @RecipientId);
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public ExistsConversationBySenderIdAndRecipientIdQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для маппинга результата в <see cref="bool"/>
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>True - если существует</returns>
        protected async override Task<bool> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("IsExists")) == 1;
            }

            return false;
        }

        #endregion

        public class Params
        {
            public int SenderId { get; set; }
            public int RecipientId { get; set; }

            public Params(int senderId, int recipientId)
            {
                SenderId = senderId;
                RecipientId = recipientId;
            }
        }
    }
}
