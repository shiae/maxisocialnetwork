﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class CreateConversationQuery : Query<int, CreateConversationQuery.Params>
    {
        #region Свойства Query 

        /// <summary>
        /// Используется для создания новой беседы
        /// </summary>
        public override string Sql => @"
            INSERT INTO Conversations(CreatedAt, UpdatedAt, CreatorId)
            VALUES (GETDATE(), GETDATE(), @CreatorId);

            SELECT CAST(scope_identity() AS int) AS ConversationId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public CreateConversationQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса CreateConversationQuery

        /// <summary>
        /// Используется для маппинга ID новой беседы
        /// </summary>
        /// <param name="reader">Курсор</param>
        protected async override Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("ConversationId"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public int CreatorId { get; set; }

            public Params(int creatorId)
            {
                CreatorId = creatorId;
            }
        }
    }
}
