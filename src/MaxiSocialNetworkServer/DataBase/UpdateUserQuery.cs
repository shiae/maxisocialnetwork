﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class UpdateUserQuery : Query<int, UpdateUserQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        public override string Sql => @"
            UPDATE 
                Users 
            SET
                FirstName = @FirstName,
                LastName = @LastName,
                ProfileImage = @ProfileImage,
                Email = @Email,
                DateOfBirth = @DateOfBirth,
                Phone = @Phone,
                Website = @Website,
                School = @School,
                Description = @Description,
                Address = @Address
            WHERE 
                Id = @UserId;

            SELECT @UserId AS Id;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public UpdateUserQuery(SqlDatabase sqlDatabase) : base(sqlDatabase) 
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Возвращает ID пользователя
        /// </summary>
        protected override async Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("Id"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public int UserId { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public byte[]? ProfileImage { get; set; }
            public string? Email { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string? Phone { get; set; }
            public string? Website { get; set; }
            public string? School { get; set; }
            public string? Description { get; set; }
            public string? Address { get; set; }

            public Params(int userId, string? firstName, string? lastName, byte[]? profileImage, string? email, DateTime? dateOfBirth, string? phone, string? website, string? school, string? description, string? address)
            {
                UserId = userId;
                FirstName = firstName;
                LastName = lastName;
                ProfileImage = profileImage;
                Email = email;
                DateOfBirth = dateOfBirth;
                Phone = phone;
                Website = website;
                School = school;
                Description = description;
                Address = address;
            }
        }
    }
}
