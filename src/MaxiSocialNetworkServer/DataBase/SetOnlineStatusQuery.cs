﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SetOnlineStatusQuery : Query<int, SetOnlineStatusQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для установки статуса пользователю, возвращает ID пользователя
        /// </summary>
        public override string Sql => @"
            UPDATE
                Users
            SET
                IsOnline = @IsOnline
            WHERE
                Id = @Id;

            SELECT @Id AS Id;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public SetOnlineStatusQuery(SqlDatabase sqlDatabase) : base(sqlDatabase)
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Используется для получения ID пользователя
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>ID пользователя</returns>
        protected async override Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("Id"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public int Id { get; set; }
            public bool IsOnline { get; set; }

            public Params(int id, bool isOnline)
            {
                Id = id;
                IsOnline = isOnline;
            }
        }
    }
}
