﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class EditMessageQuery : Query<int, EditMessageQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        public override string Sql => @"
            UPDATE
                Messages
            SET
                Text = @Text
            WHERE
                Id = @Id;
        ";

        #endregion

        #region Конструктор

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public EditMessageQuery(SqlDatabase sqlDataBase) : base(sqlDataBase)
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Используется для выполнения запроса в базе данных
        /// </summary>
        /// <returns>Результат выполнения</returns>
        public override async Task<int> Run(Params @params)
        {
            return await database.Execute(Sql, ConvertToSqlParameters(@params));
        }

        /// <summary>
        /// Используется для получения сообщения
        /// </summary>
        /// <returns>Новое сообщение</returns>
        protected override Task<int> Map(SqlDataReader reader)
        {
            throw new NotSupportedException();
        }

        #endregion

        public class Params
        {
            public int Id { get; set; }
            public string Text { get; set; }

            public Params(int id, string text)
            {
                Id = id;
                Text = text;
            }
        }
    }
}
