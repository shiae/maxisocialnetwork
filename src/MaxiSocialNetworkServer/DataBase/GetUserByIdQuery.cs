﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class GetUserByIdQuery : Query<UserDTO, GetUserByIdQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        public override string Sql => @"
            SELECT
                u.Id,
                u.Login,
                u.FirstName,
                u.LastName,
                u.Login,
                u.Password,
                u.IsOnline,
                u.Email,
                u.DateOfBirth,
                u.Phone,
                u.Website,
                u.School,
                u.Description,
                u.Address,
                u.ProfileImage
            FROM
                Users u
            WHERE
                u.Id = @UserId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public GetUserByIdQuery(SqlDatabase sqlDatabase) : base(sqlDatabase)
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Возвращает пользователя
        /// </summary>
        protected override async Task<UserDTO> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return new UserDTO()
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    Login = reader.GetString(reader.GetOrdinal("Login")),
                    Password = reader.GetString(reader.GetOrdinal("Password")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    ProfileImage = reader["ProfileImage"] is DBNull ? null : (byte[])reader["ProfileImage"],
                    Email = reader["Email"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Email")),
                    DateOfBirth = reader["DateOfBirth"] is DBNull ? null : reader.GetDateTime(reader.GetOrdinal("DateOfBirth")),
                    Phone = reader["Phone"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Phone")),
                    Website = reader["Website"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Website")),
                    School = reader["School"] is DBNull ? null : reader.GetString(reader.GetOrdinal("School")),
                    Description = reader["Description"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Description")),
                    Address = reader["Address"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Address")),
                    IsOnline = reader.GetBoolean(reader.GetOrdinal("IsOnline"))
                };
            }

            return null;
        }

        #endregion

        public class Params
        {
            public int UserId { get; set; }

            public Params(int userId)
            {
                UserId = userId;
            }
        }
    }
}
