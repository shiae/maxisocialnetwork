﻿using Microsoft.EntityFrameworkCore.SqlServer.Scaffolding.Internal;
using System.Data.SqlClient;
using System.Drawing;

namespace MaxiSocialNetworkServer
{
    public class GetChatListItemByUserLoginAndRecipientIdQuery : Query<UserChatDTO?, GetChatListItemByUserLoginAndRecipientIdQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для получения списка чатов конкретного пользователя по Логину
        /// </summary>
        public override string Sql => @"
            WITH
               lastMessages AS (SELECT
                                   m.Id AS MessageId,
                                   m.Text AS Message,
                                   m.SentAt,
                                   m.ConversationId,
                                   u.Id As UserId,
                                   u.FirstName,
                                   u.LastName,
                                   u.ProfileImage,
                                   c.HasUnreadMessages
                               FROM
                                   (SELECT
                                       (SELECT      
                                            m.Id
                                        FROM
                                            Messages m
                                        WHERE
                                            m.ConversationId = MAX(p.ConversationId)
                                        ORDER BY 
                                            m.SentAt DESC
                                        OFFSET 0 ROWS
                                        FETCH NEXT 1 ROWS ONLY) AS MessageId
                                   FROM
                                       Participants p
                                   WHERE
                                       p.UserId = @CurrentUserId
                                       AND EXISTS (SELECT 
                                                       NULL 
                                                   FROM 
                                                       Participants p2 
                                                   WHERE 
                                                       p2.ConversationId = p.ConversationId
                                                       AND p2.UserId = @ParticipateUserId)) t
                               INNER JOIN Messages m
                                   ON t.MessageId = m.Id
                               INNER JOIN Conversations c
                                   ON m.ConversationId = c.Id
                               INNER JOIN Participants p
                                   ON c.Id = p.ConversationId
                               INNER JOIN Users u
                                   ON p.UserId = u.Id
                               WHERE
                                   u.Id != @CurrentUserId)
            SELECT
               lm.MessageId,
               lm.Message,
               lm.SentAt,
               lm.ConversationId,
               lm.UserId,
               lm.FirstName,
               lm.LastName,
               lm.ProfileImage,
               lm.HasUnreadMessages,
               u.IsOnline
            FROM
               lastMessages lm
            INNER JOIN Users u
               ON lm.UserId = u.Id;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public GetChatListItemByUserLoginAndRecipientIdQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для конвертации результата запроса в список <see cref="UserChatDTO"/>
        /// </summary>
        /// <param name="reader">Результат запроса</param>
        /// <returns>Список <see cref="UserChatDTO"/></returns>
        protected override async Task<UserChatDTO?> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return new UserChatDTO
                {
                    MessageId = reader.GetValueOrDefault<int?>("MessageId"),
                    ConversationId = reader["ConversationId"] is DBNull ? null : reader.GetInt32(reader.GetOrdinal("ConversationId")),
                    UserId = reader.GetInt32(reader.GetOrdinal("UserId")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    Message = reader["Message"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Message")),
                    MessageDate = reader["SentAt"] is DBNull ? null : reader.GetDateTime(reader.GetOrdinal("SentAt")),
                    ProfileImage = reader["ProfileImage"] is DBNull ? null : (byte[])reader["ProfileImage"],
                    HasUnreadMessages = reader.GetBoolean(reader.GetOrdinal("HasUnreadMessages")),
                    IsOnline = reader.GetBoolean(reader.GetOrdinal("IsOnline")),
                    ProfileRgbImage = GenerateHexColor()
                };
            }

            return null;
        }

        #endregion

        #region Приватные методы

        /// <summary>
        /// Используется для генерации HEX цвета для аватарки пользователя, которая будет показана, если основной аватарки нет
        /// </summary>
        /// <returns>Цвет в HEX представлении</returns>
        private string GenerateHexColor()
        {
            Random random = new Random();
            Color randomColor;

            do
            {
                randomColor = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
            }
            while ((randomColor.R + randomColor.G + randomColor.B) / 3 >= 230 || randomColor.GetSaturation() < 0.3);

            return String.Format("#{0:X2}{1:X2}{2:X2}", randomColor.R, randomColor.G, randomColor.B);
        }

        #endregion

        public class Params
        {
            public int CurrentUserId { get; set; }
            public int ParticipateUserId { get; set; }

            public Params(int currentUserId, int participateUserId)
            {
                CurrentUserId = currentUserId;
                ParticipateUserId = participateUserId;
            }
        }
    }
}
