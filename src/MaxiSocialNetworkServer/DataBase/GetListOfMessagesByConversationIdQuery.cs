﻿using System.Data;
using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class GetListOfMessagesByConversationIdQuery : Query<List<MessageDTO>, GetListOfMessagesByConversationIdQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для получения списка сообщений по ID чата
        /// </summary>
        public override string Sql => @"
            SELECT
                m.Id,
                m.Text,
                m.ConversationId,
                m.SentAt,
                m.UpdatedAt,
                m.SenderId,
                f.Id AS FileId,
                f.FileData,
                f.FileName,
                f.Extension,
                f.Height,
                f.Width
            FROM
                Messages m
            LEFT JOIN Files f
                on m.Id = f.MessageId
            WHERE
                m.ConversationId = @ConversationId
                AND (m.Text IS NOT NULL OR f.Id IS NOT NULL);
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public GetListOfMessagesByConversationIdQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для маппинга результата в список <see cref="MessageDTO"/>
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>список <see cref="MessageDTO"/></returns>
        protected override async Task<List<MessageDTO>> Map(SqlDataReader reader)
        {
            List<MessageDTO> messages = new List<MessageDTO>();

            while (await reader.ReadAsync())
            {
                MessageDTO? foundMessageDTO = messages.FirstOrDefault(item => item.Id == reader.GetInt32(reader.GetOrdinal("Id")));

                if (foundMessageDTO == null)
                {
                    MessageDTO messageDTO = new MessageDTO()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        Text = reader["Text"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Text")),
                        ConversationId = reader.GetInt32(reader.GetOrdinal("ConversationId")),
                        SentAt = reader.GetDateTime(reader.GetOrdinal("SentAt")),
                        UpdatedAt = reader.GetDateTime(reader.GetOrdinal("UpdatedAt")),
                        SenderId = reader.GetInt32(reader.GetOrdinal("SenderId")),
                        Files = new List<FileDTO>()
                    };
                    messages.Add(messageDTO);
                    foundMessageDTO = messageDTO;
                }

                if (reader["FileId"] is not DBNull)
                {
                    FileDTO fileDTO = new FileDTO
                    {
                        Data = (byte[])reader.GetValue("FileData"),
                        FileName = reader.GetString(reader.GetOrdinal("FileName")),
                        Extension = reader.GetString(reader.GetOrdinal("Extension")),
                        Height = reader.GetDouble(reader.GetOrdinal("Height")),
                        Width = reader.GetDouble(reader.GetOrdinal("Width"))
                    };

                    foundMessageDTO?.Files?.Add(fileDTO);
                }
            }

            return messages;
        }

        #endregion

        /// <summary>
        /// Параметры для получения сообщений
        /// </summary>
        public class Params
        {
            public int ConversationId { get; set; }

            public Params(int conversationId)
            {
                this.ConversationId = conversationId;
            }
        }
    }
}
