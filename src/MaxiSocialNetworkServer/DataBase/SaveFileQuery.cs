﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SaveFileQuery : Query<int, SaveFileQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для сохранения картинки
        /// </summary>
        public override string Sql => @"
            INSERT INTO Files (FileData, FileName, Extension, Height, Width, MessageId)
            VALUES (@FileData, @FileName, @Extension, @Height, @Width, @MessageId);

            SELECT CAST(scope_identity() AS int) AS FileId;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public SaveFileQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Достает ID сохраненной картинки
        /// </summary>
        protected async override Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("FileId"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public byte[] FileData { get; set; }
            public string FileName { get; set; }
            public string Extension { get; set; }
            public double Height { get; set; }
            public double Width { get; set; }
            public int MessageId { get; set; }

            public Params(byte[] fileData, string fileName, string extension, double height, double width, int messageId)
            {
                FileData = fileData;
                FileName = fileName;
                Extension = extension;
                Height = height;
                Width = width;
                MessageId = messageId;
            }
        }
    }
}
