﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация абстрактного класса <see cref="Query"/>
    /// Используется для получения пользователя по Логину и Паролю
    /// </summary>
    public class GetUserByLoginAndPasswordQuery : Query<UserDTO?, GetUserByLoginAndPasswordQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для получения пользователя по Логину и Паролю
        /// </summary>
        public override string Sql => @"
            SELECT
                u.Id,
                u.Login,
                u.FirstName,
                u.LastName,
                u.Login,
                u.Password,
                u.IsOnline,
                u.Email,
                u.DateOfBirth,
                u.Phone,
                u.Website,
                u.School,
                u.Description,
                u.Address,
                u.ProfileImage
            FROM
                Users u
            WHERE
                u.Login = @Login
                AND u.Password = @Password;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public GetUserByLoginAndPasswordQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Используется для маппинга данных из SQL в сущность <see cref="UserDTO"/>
        /// </summary>
        /// <param name="reader">Курсор</param>
        /// <returns>Сущность <see cref="UserDTO"/></returns>
        protected override async Task<UserDTO?> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return new UserDTO()
                {
                    Id = reader.GetInt32(reader.GetOrdinal("Id")),
                    Login = reader.GetString(reader.GetOrdinal("Login")),
                    Password = reader.GetString(reader.GetOrdinal("Password")),
                    FirstName = reader.GetString(reader.GetOrdinal("FirstName")),
                    LastName = reader.GetString(reader.GetOrdinal("LastName")),
                    ProfileImage = reader["ProfileImage"] is DBNull ? null : (byte[])reader["ProfileImage"],
                    Email = reader["Email"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Email")),
                    DateOfBirth = reader["DateOfBirth"] is DBNull ? null : reader.GetDateTime(reader.GetOrdinal("DateOfBirth")),
                    Phone = reader["Phone"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Phone")),
                    Website = reader["Website"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Website")),
                    School = reader["School"] is DBNull ? null : reader.GetString(reader.GetOrdinal("School")),
                    Description = reader["Description"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Description")),
                    Address = reader["Address"] is DBNull ? null : reader.GetString(reader.GetOrdinal("Address")),
                    IsOnline = reader.GetBoolean(reader.GetOrdinal("IsOnline"))
                };
            }

            return null;
        }

        #endregion

        /// <summary>
        /// Вложенный класс, определяющий параметры для запроса <see cref="Sql"/>
        /// </summary>
        public class Params
        {
            public string? Login { get; private set; }
            public string? Password { get; private set; }

            public Params(string? login, string? password)
            {
                Login = login;
                Password = password;
            }
        }
    }
}
