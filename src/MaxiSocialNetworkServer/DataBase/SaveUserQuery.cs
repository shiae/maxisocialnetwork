﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SaveUserQuery : Query<int, SaveUserQuery.Params>
    {
        #region Свойства класса Query

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        public override string Sql => @"
            INSERT INTO Users (
                Login,
                Password,
                FirstName,
                LastName,
                IsOnline,
                RoleId)
            VALUES (
                @Login,
                @Password,
                @FirstName,
                @LastName,
                @IsOnline,
                1);
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="database">База данных</param>
        public SaveUserQuery(SqlDatabase database) : base(database)
        {

        }

        #endregion

        #region Методы класса Query

        /// <summary>
        /// Сохраняет пользователя в базу данных
        /// </summary>
        /// <param name="params">Пользователь</param>
        /// <returns>Кол</returns>
        /// <exception cref="NotImplementedException"></exception>
        public override async Task<int> Run(Params @params)
        {
            return await database.Execute(Sql, ConvertToSqlParameters(@params));
        }

        /// <summary>
        /// Не поддерживается в текущей реализации
        /// </summary>
        /// <exception cref="NotImplementedException">В случае вызова</exception>
        protected override Task<int> Map(SqlDataReader reader)
        {
            throw new NotSupportedException();
        }

        #endregion

        /// <summary>
        /// Хранит в себе пользователя, который будет сохранен в БД
        /// </summary>
        public class Params
        {
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Login { get; set; }
            public string? Password { get; set; }
            public bool IsOnline { get; set; }

            public Params(UserDTO user)
            {
                FirstName = user.FirstName;
                LastName = user.LastName;
                Login = user.Login;
                Password = user.Password;
                IsOnline = user.IsOnline;
            }
        }
    }
}
