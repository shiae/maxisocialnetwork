﻿using System.Data.SqlClient;

namespace MaxiSocialNetworkServer
{
    public class SetUnreadMessagesStatusConversationQuery : Query<int, SetUnreadMessagesStatusConversationQuery.Params>
    {
        #region Свойства Query

        /// <summary>
        /// Устанавливает HasUnreadMessages статус для беседы
        /// </summary>
        public override string Sql => @"
            UPDATE 
                Conversations 
            SET 
                HasUnreadMessages = @HasUnreadMessages 
            WHERE 
                Id = @ConversationId;

            SELECT @ConversationId AS Id;
        ";

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public SetUnreadMessagesStatusConversationQuery(SqlDatabase sqlDatabase) : base(sqlDatabase)
        {

        }

        #endregion

        #region Методы Query

        /// <summary>
        /// Возвращает ID измененной беседы
        /// </summary>
        protected override async Task<int> Map(SqlDataReader reader)
        {
            if (await reader.ReadAsync())
            {
                return reader.GetInt32(reader.GetOrdinal("Id"));
            }

            return 0;
        }

        #endregion

        public class Params
        {
            public int ConversationId { get; set; }
            public bool HasUnreadMessages { get; set; }

            public Params(int conversationid, bool hasUnreadMessages)
            {
                ConversationId = conversationid;
                HasUnreadMessages = hasUnreadMessages;
            }
        }
    }
}
