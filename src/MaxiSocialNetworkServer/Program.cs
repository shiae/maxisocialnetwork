using MaxiSocialNetworkServer;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

/* OTHERS */
builder.Services.AddScoped(provider => new SqlDatabase(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddSingleton<ConnectionContext>();
builder.Services.AddControllers();
builder.Services.AddSignalR(o =>
{
    o.EnableDetailedErrors = true;
    o.MaximumReceiveMessageSize = null;
});

/* QUERIES */
builder.Services.AddScoped<ExistsUserByLoginQuery>();
builder.Services.AddScoped<GetChatListOfUserByLoginQuery>();
builder.Services.AddScoped<GetUserByLoginAndPasswordQuery>();
builder.Services.AddScoped<SaveUserQuery>();
builder.Services.AddScoped<GetSectionRightsOfUserByLoginQuery>();
builder.Services.AddScoped<GetListOfMessagesByConversationIdQuery>();
builder.Services.AddScoped<SetOnlineStatusQuery>();
builder.Services.AddScoped<SaveMessageQuery>();
builder.Services.AddScoped<GetMessageByIdQuery>();
builder.Services.AddScoped<ExistsConversationBySenderIdAndRecipientIdQuery>();
builder.Services.AddScoped<CreateConversationQuery>();
builder.Services.AddScoped<SaveFileQuery>();
builder.Services.AddScoped<EditMessageQuery>();
builder.Services.AddScoped<DeleteMessageByIdQuery>();
builder.Services.AddScoped<AddParticipantToConversationQuery>();
builder.Services.AddScoped<SetUnreadMessagesStatusConversationQuery>();
builder.Services.AddScoped<UpdateUserQuery>();
builder.Services.AddScoped<GetUserByIdQuery>();
builder.Services.AddScoped<GetChatListItemByUserLoginAndRecipientIdQuery>();
builder.Services.AddScoped<SetIsOnlineStatusForAllUsersQuery>();

/* REPOSITORIES */
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IMessageRepository, MessageRepository>();
builder.Services.AddScoped<IConversationRepository, ConversationRepository>();

/* SERVICES */
builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IMessageService, MessageService>();
builder.Services.AddScoped<IConversationService, ConversationService>();

/* MAPPERS */
builder.Services.AddScoped<MessageDTOMapper>();

/* HUBS */
builder.Services.AddScoped<UserHub>();

/* CONFIGURE APP */
var app = builder.Build();
app.MapControllers();
app.UseCors("SignalRClientPolicy");
app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
    endpoints.MapHub<UserHub>("/UserHub");
});
app.UseMiddleware<ErrorHandlingMiddleware>();

// ������������� ���� ������������� ������ � ������� ��� ������� �������
try
{
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;
        SetIsOnlineStatusForAllUsersQuery setIsOnlineStatusForAllUsersQuery = services.GetRequiredService<SetIsOnlineStatusForAllUsersQuery>();
        await setIsOnlineStatusForAllUsersQuery.Run(new SetIsOnlineStatusForAllUsersQuery.Params(false));
    }
}
catch (Exception)
{
    // ignored
}

app.Run();