﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IUserService"/>
    /// </summary>
    public class UserService : IUserService
    {
        #region Глобальные константы

        public const string CHAT_LIST_NOT_FOUND_ERROR_MSG = "Не было найдено ни одного чата!";

        #endregion

        #region Приватные поля

        /// <summary>
        /// Используется для работы с базой данных
        /// </summary>
        private readonly IUserRepository _userRepository;

        #endregion

        #region Конструкторы 

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="userRepository">Используется для работы с базой данных</param>
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        #endregion

        #region Методы интерфейса IUserService

        /// <summary>
        /// Используется для проверки существования пользователя по логину в базе данных
        /// </summary>
        /// <param name="userDTO">Хранит логин</param>
        /// <returns>True - если существует по логину</returns>
        public async Task<bool> ExistsUserByLogin(UserDTO userDTO)
        {
            return await _userRepository.ExistsUserByLogin(userDTO);
        }

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        /// <param name="userDTO">Пользователь</param>
        /// <returns>Количество затронутых строк</returns>
        public async Task<int> SaveUser(UserDTO userDTO)
        {
            return await _userRepository.SaveUser(userDTO);
        }

        /// <summary>
        /// Используется для получения пользователя по логину и паролю
        /// </summary>
        /// <param name="userDTO">Хранит логин и пароль</param>
        /// <returns>Найденный пользователь</returns>
        public async Task<UserDTO?> GetUserByLoginAndPassword(UserDTO userDTO)
        {
            return await _userRepository.GetUserByLoginAndPassword(userDTO);
        }

        /// <summary>
        /// Метод используется для получения списка чатов
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список чатов</returns>
        public async Task<List<UserChatDTO>> GetChatList(string login)
        {
            return await _userRepository.GetChatList(login);
        }

        /// <summary>
        /// Используется для получения прав на все секции по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав</returns>
        public async Task<List<SectionRightDTO>> GetSectionRights(string login)
        {
            return await _userRepository.GetSectionRights(login);
        }

        /// <summary>
        /// Используется для установки онлайн статуса пользователю
        /// </summary>
        /// <param name="isOnline">True - если пользователь онлайн</param>
        /// <param name="id">ID пользователя</param>
        public async Task SetOnlineStatus(int id, bool isOnline)
        {
            await _userRepository.SetOnlineStatus(id, isOnline);
        }

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информция</param>
        /// <returns>ID пользователя измененного</returns>
        public async Task<int> UpdateUser(UserDTO userDTO)
        {
            return await _userRepository.UpdateUser(userDTO);
        }

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        public async Task<UserDTO> GetUserById(int id)
        {
            return await _userRepository.GetUserById(id);
        }

        /// <summary>
        /// Используется для получения элемента в чате по ID текущего пользователя и ID собеседника
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        public async Task<UserChatDTO?> GetChatListItemByCurrentUserAndParticipateUserId(int currentUserId, int participateUserId)
        {
            return await _userRepository.GetChatListItemByCurrentUserAndParticipateUserId(currentUserId, participateUserId);
        }

        #endregion
    }
}
