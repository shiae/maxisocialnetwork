﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IConversationService"/>
    /// </summary>
    public class ConversationService : IConversationService
    {
        #region Глобальные константы

        public const string SAVE_MESSAGE_ERROR_MSG = "Произошло ошибка при сохранении сообщения: {0}";
        public const string CREATE_CONVERSATION_ERROR_MS = "Произошло ошибка создании беседы";

        #endregion

        #region Приватные поля

        /// <summary>
        /// Репозиторий для работы с беседами
        /// </summary>
        private readonly IConversationRepository _conversationRepository;

        /// <summary>
        /// Используется для работы с сообщениями
        /// </summary>
        private readonly IMessageService _messageService;

        /// <summary>
        /// Используется для маппинга в <see cref="MessageDTO"/>
        /// </summary>
        private readonly MessageDTOMapper _messageDTOMapper;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ConversationService(IConversationRepository conversationRepository,
                                   IMessageService messageService, 
                                   MessageDTOMapper messageDTOMapper)
        {
            _conversationRepository = conversationRepository;
            _messageService = messageService;
            _messageDTOMapper = messageDTOMapper;
        }

        #endregion

        #region Методы интерфейса IConversationService

        /// <summary>
        /// Определяет, существует ли беседа по ID отправителя и ID получателя
        /// </summary>
        /// <param name="senderId">ID отправителя</param>
        /// <param name="recipientId">ID получателя</param>
        /// <returns>True - существует</returns>
        public async Task<bool> ExistsBySenderIdAndRecipientId(int senderId, int recipientId)
        {
            return await _conversationRepository.ExistsBySenderIdAndRecipientId(senderId, recipientId);
        }

        /// <summary>
        /// Используется для создания новой беседы
        /// </summary>
        /// <param name="creatorId">ID создателя</param>
        /// <returns>ID новой беседы</returns>
        public async Task<int> CreateConversation(int creatorId)
        {
            int id = await _conversationRepository.CreateConversation(creatorId);

            if (id == 0)
            {
                throw new ServerException(CREATE_CONVERSATION_ERROR_MS);
            }

            return id;
        }

        /// <summary>
        /// Используется для сохранения отправленного в беседу сообщения
        /// </summary>
        /// <param name="message">Новое сообщение</param>
        /// <returns>Сохраненное сообщение</returns>
        public async Task<MessageDTO> SaveMessage(SendMessageDTO message)
        {
            if (message.ConversationId == null || message.ConversationId == 0)
            {
                message.ConversationId = await CreateConversation(message.SenderId);
                await AddParticipant(message.SenderId, (int)message.ConversationId!);
                await AddParticipant(message.RecipientId, (int)message.ConversationId!);
            }

            MessageDTO? savedMessage = await _messageService.SaveMessage(_messageDTOMapper.ToMessageDTO(message));

            if (savedMessage != null && (savedMessage?.Files?.Count > 0 || !string.IsNullOrEmpty(savedMessage?.Text)))
            {
                await EditHasUnreadMessageStatus((int) message.ConversationId, true);

                return savedMessage;
            }
            else
            {
                throw new ServerException(string.Format(SAVE_MESSAGE_ERROR_MSG, message.Text));
            }
        }

        /// <summary>
        /// Используется для добавления нового участника в беседу
        /// </summary>
        /// <param name="userId">ID пользователя</param>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>ID новоего участника</returns>
        public async Task<int> AddParticipant(int userId, int conversationId)
        {
            return await _conversationRepository.AddParticipant(userId, conversationId);
        }

        /// <summary>
        /// Используется для изменения статуса прочитанности сообщений беседы по ID
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="hasUnreadMessages">True - имеет непрочитанные сообщения</param>
        /// <returns>ID беседы</returns>
        public async Task<int> EditHasUnreadMessageStatus(int conversationId, bool hasUnreadMessages)
        {
            return await _conversationRepository.EditHasUnreadMessageStatus(conversationId, hasUnreadMessages);
        }

        #endregion
    }
}
