﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IMessageService"/>
    /// </summary>
    public class MessageService : IMessageService
    {
        #region Глобальные константы

        public const string INVALID_MESSAGE_INFORMATION_ERROR_MSG = "Не удалось сохранить сообщение: {0}. В беседу: {1}. Создатель: {2}";

        #endregion

        #region Приватные поля

        /// <summary>
        /// Используется для работы с сообщениями
        /// </summary>
        private readonly IMessageRepository _messageRepository;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public MessageService(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        #endregion

        #region Методы IMessageService

        /// <summary>
        /// Используется для получения списка сообщение по ID беседы
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <returns>Список сообщений</returns>
        public async Task<List<MessageDTO>> GetAllByConversationId(int conversationId)
        {
            return await _messageRepository.GetAllByConversationId(conversationId);
        }

        /// <summary>
        /// Используется для сохранения сообщения в базу данных
        /// </summary>
        /// <param name="message">Сообщение</param>
        public async Task<MessageDTO?> SaveMessage(MessageDTO message)
        {
            if (message.ConversationId == 0 || 
                message.SenderId == 0)
            {
                throw new ServerException(string.Format(INVALID_MESSAGE_INFORMATION_ERROR_MSG, message.Text, message.ConversationId, message.SenderId));
            }

            int id = await _messageRepository.SaveMessage(message);
            return await _messageRepository.GetMessageById(id);
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        /// <param name="editMessageDTO">Новое сообщение</param>
        /// <returns>Измененное изменение</returns>
        public async Task<MessageDTO> EditMessage(EditMessageDTO editMessageDTO)
        {
            MessageDTO? messageDTO = await _messageRepository.EditMessage(editMessageDTO);

            if (messageDTO == null)
            {
                throw new ServerException("Не удалось изменить сообщение!");
            }

            return messageDTO;
        }

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        public async Task<MessageDTO> DeleteMessageById(int id)
        {
            MessageDTO? messageDTO = await _messageRepository.GetMessageById(id);

            if (messageDTO == null)
            {
                throw new ServerException("Не удалось удалить сообщение, потому что его нет!");
            }

            await _messageRepository.DeleteMessageById(id);

            return messageDTO!;
        }

        #endregion
    }
}
