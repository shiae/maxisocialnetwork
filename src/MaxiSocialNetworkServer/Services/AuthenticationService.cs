﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Реализация интерфейса <see cref="IAuthenticationService"/>
    /// </summary>
    public class AuthenticationService : IAuthenticationService
    {
        #region Глобальные константы

        public const string INVALID_REGISTRATION_INFORMATION_ERROR_MSG = "Пожалуйста, предоставьте валидную регистрационную информацию!";
        public const string USER_ALREADY_EXISTS_ERROR_MSG = "Пользователь с таким логином уже существует в системе!";
        public const string INVALID_AUTHORIZATION_INFORMATION_ERROR_MSG = "Пожалуйста, предоставьте валидную регистрационную информацию!";
        public const string USER_NOT_FOUND_ERROR_MSG = "Пользователь с таким логином не существует в системе!";
        public const string INCORRECT_PASSWORD_ERROR_MSG = "Был введен неправильный пароль. Проверьте его, пожалуйста.";

        #endregion

        #region Приватные поля

        /// <summary>
        /// Репозиторий, использующийся для выполнения действий над пользователем в БД
        /// </summary>
        private readonly IUserService _userService;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="context">Контекст для работы с БД</param>
        /// <param name="userMapper">Реализация паттерна Mapper для преобразования из <see cref="UserDTO"/> в <see cref="User"/> и обратно</param>
        public AuthenticationService(IUserService userService)
        {
            _userService = userService;
        }

        #endregion

        #region Методы интерфейса IAuthenticationService

        /// <summary>
        /// Метод, использующийся для регистрации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Пользователь, которого нужно зарегистрировать в системе</param>
        public async Task RegisterUser(UserDTO userDTO)
        {
            if (string.IsNullOrEmpty(userDTO.Login) ||
                string.IsNullOrEmpty(userDTO.Password) ||
                string.IsNullOrEmpty(userDTO.FirstName) ||
                string.IsNullOrEmpty(userDTO.LastName))
            {
                throw new ServerException(INVALID_REGISTRATION_INFORMATION_ERROR_MSG);
            }

            if (await _userService.ExistsUserByLogin(userDTO))
            {
                throw new ServerException(USER_ALREADY_EXISTS_ERROR_MSG);
            }

            await _userService.SaveUser(userDTO);
        }

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Данные пользователя для авторизации</param>
        /// <returns>Авторизованного пользователя</returns>
        public async Task<UserDTO> LoginUser(UserDTO userDTO)
        {
            if (string.IsNullOrEmpty(userDTO.Login) ||
                string.IsNullOrEmpty(userDTO.Password))
            {
                throw new ServerException(INVALID_AUTHORIZATION_INFORMATION_ERROR_MSG);
            }

            if (!await _userService.ExistsUserByLogin(userDTO))
            {
                throw new ServerException(USER_NOT_FOUND_ERROR_MSG);
            }

            UserDTO? user = await _userService.GetUserByLoginAndPassword(userDTO);

            if (user == null)
            {
                throw new ServerException(INCORRECT_PASSWORD_ERROR_MSG);
            }

            if (user.IsOnline)
            {
                throw new ServerException("Такой пользователь уже в сети. Если произошла какая то проблема, обратитесь к администратору!");
            }

            await _userService.SetOnlineStatus((int)user.Id!, true);

            return user;
        }

        #endregion
    }
}
