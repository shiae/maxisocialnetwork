﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Интерфейс, предоставляющий меторды для реализации аутентификации
    /// </summary>
    public interface IAuthenticationService
    {
        /// <summary>
        /// Метод, использующийся для регистрации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Пользователь, которого нужно зарегистрировать в системе</param>
        Task RegisterUser(UserDTO userDTO);

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Данные пользователя для авторизации</param>
        /// <returns>Авторизованного пользователя</returns>
        Task<UserDTO> LoginUser(UserDTO userDTO);
    }
}
