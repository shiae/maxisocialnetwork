﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Интерфейс, предоставляющий методы для реализации бизнес логики, связанной с работой с пользователем
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Используется для проверки существования пользователя по логину в базе данных
        /// </summary>
        /// <param name="userDTO">Хранит логин</param>
        /// <returns>True - если существует по логину</returns>
        Task<bool> ExistsUserByLogin(UserDTO userDTO);

        /// <summary>
        /// Используется для сохранения пользователя в базу данных
        /// </summary>
        /// <param name="userDTO">Пользователь</param>
        /// <returns>Количество затронутых строк</returns>
        Task<int> SaveUser(UserDTO userDTO);

        /// <summary>
        /// Используется для получения пользователя по логину и паролю
        /// </summary>
        /// <param name="userDTO">Хранит логин и пароль</param>
        /// <returns>Найденный пользователь</returns>
        Task<UserDTO?> GetUserByLoginAndPassword(UserDTO userDTO);

        /// <summary>
        /// Метод используется для получения списка чатов
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список чатов</returns>
        Task<List<UserChatDTO>> GetChatList(string login);

        /// <summary>
        /// Используется для получения прав на все секции по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав</returns>
        Task<List<SectionRightDTO>> GetSectionRights(string login);

        /// <summary>
        /// Используется для установки онлайн статуса пользователю
        /// </summary>
        /// <param name="isOnline">True - если пользователь онлайн</param>
        /// <param name="id">ID пользователя</param>
        Task SetOnlineStatus(int id, bool isOnline);

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информция</param>
        /// <returns>ID пользователя измененного</returns>
        Task<int> UpdateUser(UserDTO userDTO);

        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        Task<UserDTO> GetUserById(int id);

        /// <summary>
        /// Используется для получения элемента в чате по ID текущего пользователя и ID собеседника
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        Task<UserChatDTO?> GetChatListItemByCurrentUserAndParticipateUserId(int currentUserId, int participateUserId);
    }
}
