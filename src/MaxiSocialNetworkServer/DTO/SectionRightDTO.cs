﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Хранит информацию о праве пользователя на секцию
    /// </summary>
    public class SectionRightDTO
    {
        /// <summary>
        /// Имя секции
        /// </summary>
        public string? SectionName { get; set; }

        /// <summary>
        /// True - имеет право (видеть)
        /// </summary>
        public bool HasAccess { get; set; }
    }
}
