﻿namespace MaxiSocialNetworkServer
{
    public class MessageDTOMapper
    {
        /// <summary>
        /// Используется для конвертации в <see cref="SendMessageDTO"/>
        /// </summary>
        public MessageDTO ToMessageDTO(SendMessageDTO sendMessageDTO)
        {
            return new MessageDTO()
            {
                SenderId = sendMessageDTO.SenderId,
                Text = sendMessageDTO.Text,
                ConversationId = sendMessageDTO.ConversationId,
                Files = sendMessageDTO.Files
            };
        }

        /// <summary>
        /// Используется для конвертации в <see cref="MessageDTO"/>
        /// </summary>
        public EditMessageDTO ToEditMessageDTO(MessageDTO? messageDTO, int flag = 0)
        {
            return new EditMessageDTO()
            {
                Message = messageDTO?.Text,
                ConversationId = messageDTO?.ConversationId,
                Id = messageDTO?.Id,
                Flag = flag
            };
        }

        /// <summary>
        /// Используется для конвертации в <see cref="EditMessageDTO"/>
        /// </summary>
        public EditMessageDTO ToEditMessageDTO(int id, int flag, int? conversationId)
        {
            return new EditMessageDTO()
            {
                Id = id,
                Flag = flag,
                ConversationId = conversationId
            };
        }
    }
}
