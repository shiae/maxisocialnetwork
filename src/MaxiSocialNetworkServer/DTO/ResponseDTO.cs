﻿namespace MaxiSocialNetworkServer.DTO
{
    /// <summary>
    /// Основная сущность для отправки результата
    /// </summary>
    public class ResponseDTO
    {
        private TypesOfExceptions _typeOfException;

        /// <summary>
        /// True - если Hub успешно отработал 
        /// </summary>
        public bool IsSuccess { get; set; } = true;

        /// <summary>
        /// В случае когда <see cref="IsSuccess"/> равен False, то здесь храним причину
        /// </summary>
        public string? ErrorMessage { get; set; }

        /// <summary>
        /// Тип исключения, если он есть
        /// </summary>
        public TypesOfExceptions? TypeOfException { get; set; }

        /// <summary>
        /// Тело ответа, здесь можем хранить любой обьект
        /// </summary>
        public object? Result { get; set; }

    }
}
