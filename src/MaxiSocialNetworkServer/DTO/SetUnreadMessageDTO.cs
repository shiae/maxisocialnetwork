﻿namespace MaxiSocialNetworkServer
{
    public class SetUnreadMessageDTO
    {
        /// <summary>
        /// ID беседы
        /// </summary>
        public int ConversationId { get; set; }

        /// <summary>
        /// True - имеются непрочитанные сообщения
        /// </summary>
        public bool HasUnreadMessages { get; set; }
    }
}
