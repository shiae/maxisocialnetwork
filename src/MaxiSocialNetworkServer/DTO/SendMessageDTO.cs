﻿namespace MaxiSocialNetworkServer
{
    public class SendMessageDTO
    {
        /// <summary>
        /// ID отправителя
        /// </summary>
        public int SenderId { get; set; }

        /// <summary>
        /// ID получателя
        /// </summary>
        public int RecipientId { get; set; }

        /// <summary>
        /// ID беседы
        /// </summary>
        public int? ConversationId { get; set; }

        /// <summary>
        /// Содержание сообщения
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Используется для хранения файлов к сообщению
        /// </summary>
        public List<FileDTO> Files { get; set; }
    }
}
