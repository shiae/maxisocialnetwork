﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// DTO, хранящее всю необходимую информацию для элемента из списка чатов
    /// </summary>
    public class UserChatDTO
    {
        /// <summary>
        /// ID сообщения
        /// </summary>
        public int? MessageId { get; set; }

        /// <summary>
        /// ID чата
        /// </summary>
        public int? ConversationId { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Имя пользователя в каждом чате
        /// </summary>
        public string? FirstName { get; set; }

        /// <summary>
        /// Фамилия пользователя в каждом чате
        /// </summary>
        public string? LastName { get; set; }

        /// <summary>
        /// Последнее сообщение, отправленное в чате
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// Дата отправки последнего сообщения
        /// </summary>
        public DateTime? MessageDate { get; set; }

        /// <summary>
        /// Картинка пользователя
        /// </summary>
        public byte[]? ProfileImage { get; set; }

        /// <summary>
        /// RGB цвет картинки пользователя
        /// </summary>
        public string? ProfileRgbImage { get; set; }

        /// <summary>
        /// Является ли пользователь в текущий момент онлайн
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// True - беседа имеет непрочитанные сообщения
        /// </summary>
        public bool HasUnreadMessages { get; set; }
    }
}
