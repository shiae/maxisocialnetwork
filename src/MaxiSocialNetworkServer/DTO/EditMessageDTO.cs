﻿namespace MaxiSocialNetworkServer
{
    public class EditMessageDTO
    {
        /// <summary>
        /// ID сообщения
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Измененнное сообщение
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// ID беседы
        /// </summary>
        public int? ConversationId { get; set; }

        /// <summary>
        /// 0 - изменен
        /// 1 - удален
        /// </summary>
        public int Flag { get; set; }
    }
}
