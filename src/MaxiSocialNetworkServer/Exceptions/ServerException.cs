﻿namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Наследуется от класса <see cref="Exception"/>
    /// Используется при ошибках во время аутентификации
    /// </summary>
    public class ServerException : Exception
    {
        public ServerException(string? message = null) : base(message)
        {

        }
    }
}
