﻿using Microsoft.AspNetCore.SignalR;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Используется в качестве глобального обработчика ошибок
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        /// <summary>
        /// Следующий Middleware
        /// </summary>
        private readonly RequestDelegate next;

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// Вызывается в качестве звена в цепи приложения
        /// </summary>
        public async Task InvokeAsync(HttpContext context, IHubContext<UserHub> hubContext)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await hubContext.Clients.All.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }
    }
}
