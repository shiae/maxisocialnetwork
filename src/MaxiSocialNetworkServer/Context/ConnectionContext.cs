﻿using System.Collections.Concurrent;

namespace MaxiSocialNetworkServer
{
    public class ConnectionContext
    {
        #region Приватные поля

        /// <summary>
        /// Словарь ConnectionId - UserId, Хранит сопоставление ID пользователя к каждой сессии
        /// </summary>
        private readonly ConcurrentDictionary<string, int> _connections = new ConcurrentDictionary<string, int>();

        #endregion

        #region Методы

        /// <summary>
        /// Используется для добавления ConnectionId и ID пользователя
        /// </summary>
        /// <param name="connectionId">ID подключения</param>
        /// <param name="userId">ID пользователя</param>
        public void AddUserId(string connectionId, int userId)
        {
            _connections.TryAdd(connectionId, userId);
        }

        /// <summary>
        /// Используется для удаления ConnectionId и ID пользователя
        /// </summary>
        /// <param name="connectionId">ID подключения</param>
        /// <returns>ID пользователя</returns>
        public int RemoveUserId(string connectionId)
        {
            int userId;
            _connections.TryRemove(connectionId, out userId);

            return userId;
        }

        /// <summary>
        /// Используется для получения ID пользователя
        /// </summary>
        /// <param name="connectionId">ID подключения</param>
        /// <returns>ID пользователя</returns>
        public string GetConnectionId(int userId)
        {
            return _connections.FirstOrDefault(x => x.Value == userId).Key;
        }

        #endregion
    }
}
