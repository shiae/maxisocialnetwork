﻿using MaxiSocialNetworkServer.DTO;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNetCore.SignalR;
using HubMethodNameAttribute = Microsoft.AspNetCore.SignalR.HubMethodNameAttribute;

namespace MaxiSocialNetworkServer
{
    /// <summary>
    /// Наследуется от класса <see cref="Hub"/>
    /// Определяет Hub, определяющий методы для работы с пользователем в системе
    /// </summary>
    [HubName("UserHub")]
    public class UserHub : Hub
    {
        #region Приватные поля

        /// <summary>
        /// Хранит в себе бизнес логику по работе с пользователем
        /// </summary>
        private readonly IUserService _userService;

        /// <summary>
        /// Хранит в себе бизнес логику по работе с сообщениями
        /// </summary>
        private readonly IMessageService _messageService;

        /// <summary>
        /// Хранит в себе бизнес логику по работе с беседами
        /// </summary>
        private readonly IConversationService _conversationService;

        /// <summary>
        /// Паттерн Service для реализации бизнес логики аутентификации
        /// </summary>
        private readonly IAuthenticationService _authenticationService;

        /// <summary>
        /// Словарь ConnectionId - UserId, Хранит сопоставление ID пользователя к каждой сессии
        /// </summary>
        private readonly ConnectionContext _connectionContext;

        /// <summary>
        /// Маппер
        /// </summary>
        private readonly MessageDTOMapper _messageMapper;

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        /// <param name="userService">Хранит в себе бизнес логику</param>
        public UserHub(IUserService userService,
                       IMessageService messageService,
                       IConversationService conversationService,
                       IAuthenticationService authenticationService,
                       ConnectionContext connectionContext,
                       MessageDTOMapper messageMapper)
        {
            _userService = userService;
            _messageService = messageService;
            _conversationService = conversationService;
            _connectionContext = connectionContext;
            _authenticationService = authenticationService;
            _messageMapper = messageMapper;
        }

        #endregion

        #region Методы Hub

        /// <summary>
        /// Используется для установки пользователю статус оффлайн при выходе из приложения
        /// </summary>
        /// <param name="exception"></param>
        /// <returns></returns>
        public async override Task OnDisconnectedAsync(Exception? exception)
        {
            int deletedUserId = _connectionContext.RemoveUserId(Context.ConnectionId);
            await _userService.SetOnlineStatus(deletedUserId, false);
            await Clients.All.SendCoreAsync("UserDisconnected", new object[] { deletedUserId });
            await base.OnDisconnectedAsync(exception);
        }

        #endregion

        #region Методы

        /// <summary>
        /// Метод, использующийцся для реализации регистрации пользователя.
        /// </summary>
        /// <param name="userDTO">Пользовательские данные для регистрации</param>
        /// <returns>Результат регистрации</returns>
        [HubMethodName("RegisterUser")]
        public async Task<ResponseDTO> RegisterUser(UserDTO userDTO)
        {
            try
            {
                await _authenticationService.RegisterUser(userDTO);
                return new ResponseDTO();
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Метод, использующийся для авторизации пользователя в системе
        /// </summary>
        /// <param name="userDTO">Пользовательские данные для авторизации</param>
        /// <returns>Ответ с результатом в качестве авторизованного пользователя</returns>
        [HubMethodName("LoginUser")]
        public async Task<ResponseDTO> LoginUser(UserDTO userDTO)
        {
            try
            {
                UserDTO user = await _authenticationService.LoginUser(userDTO);
                user.ConnectionId = Context.ConnectionId;

                _connectionContext.AddUserId(user.ConnectionId, (int)user.Id!);

                await Clients.All.SendCoreAsync("UserConnected", new object[] { user.Id });

                return new ResponseDTO()
                {
                    IsSuccess = true,
                    Result = user
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Используется для получения списка чатов по логину пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список чатов</returns>
        [HubMethodName("GetUserChatListByLogin")]
        public async Task<ResponseDTO> GetUserChatListByLogin(string login)
        {
            try
            {
                List<UserChatDTO> chatList = await _userService.GetChatList(login);
                return new ResponseDTO
                {
                    IsSuccess = true,
                    Result = chatList
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Используется для получения списка прав на секции для пользователя по логину
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <returns>Список прав на секции</returns>
        [HubMethodName("GetSectionRightsOfUserByLogin")]
        public async Task<ResponseDTO> GetSectionRightsOfUserByLogin(string login)
        {
            try
            {
                List<SectionRightDTO> sectionRights = await _userService.GetSectionRights(login);
                return new ResponseDTO
                {
                    IsSuccess = true,
                    Result = sectionRights
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Используется для получения списка сообщения по ID чата
        /// </summary>
        /// <param name="conversationId">ID чата</param>
        /// <returns>Список сообщений</returns>
        [HubMethodName("GetListOfMessagesByConversationId")]
        public async Task<ResponseDTO> GetAllByConversationId(int conversationId)
        {
            try
            {
                List<MessageDTO> messages = await _messageService.GetAllByConversationId(conversationId);
                return new ResponseDTO
                {
                    IsSuccess = true,
                    Result = messages
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Используется для отпавки сообщения клиенту
        /// </summary>
        /// <param name="connectionId">Идентификатор клиента</param>
        /// <param name="message">Содержание сообщения</param>
        [HubMethodName("SendMessage")]
        public async Task SendMessage(SendMessageDTO sendedMessage)
        {
            try
            {
                MessageDTO message = await _conversationService.SaveMessage(sendedMessage);
                await Clients
                        .Clients(new List<string>() { _connectionContext.GetConnectionId(message.SenderId), _connectionContext.GetConnectionId((int)message.RecipientId!) })
                        .SendCoreAsync("ReceiveMessage", new object[] { message });
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }

        /// <summary>
        /// Используется для изменения сообщения
        /// </summary>
        [HubMethodName("EditMessage")]
        public async Task EditMessage(EditMessageDTO editMessageDTO)
        {
            try
            {
                MessageDTO message = await _messageService.EditMessage(editMessageDTO);
                await Clients
                        .Clients(new List<string>() { _connectionContext.GetConnectionId(message.SenderId), _connectionContext.GetConnectionId((int)message.RecipientId!) })
                        .SendCoreAsync("ChangedMessage", new object[] { _messageMapper.ToEditMessageDTO(message) });
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }

        /// <summary>
        /// Используется для удаления сообщения
        /// </summary>
        /// <param name="id">ID сообщения</param>
        [HubMethodName("DeleteMessage")]
        public async Task DeleteMessage(int id)
        {
            try
            {
                MessageDTO deletedMessage = await _messageService.DeleteMessageById(id);
                await Clients
                        .Clients(new List<string>() { _connectionContext.GetConnectionId(deletedMessage.SenderId), _connectionContext.GetConnectionId((int)deletedMessage.RecipientId!) })
                        .SendCoreAsync("ChangedMessage", new object[] { _messageMapper.ToEditMessageDTO(id, 1, deletedMessage.ConversationId) });
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }

        /// <summary>
        /// Используеться для установки статуса непрочитанности сообщений
        /// </summary>
        /// <param name="conversationId">ID беседы</param>
        /// <param name="conversationId">True - имеет непрочитанные сообщения</param>
        [HubMethodName("EditHasUnreadMessageStatus")]
        public async Task EditHasUnreadMessageStatus(SetUnreadMessageDTO setUnreadMessageDTO)
        {
            try
            {
                await _conversationService.EditHasUnreadMessageStatus(setUnreadMessageDTO.ConversationId, setUnreadMessageDTO.HasUnreadMessages);
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }

        /// <summary>
        /// Используется для обновления пользовательской информации
        /// </summary>
        /// <param name="userDTO">Пользовательская информация</param>
        [HubMethodName("UpdateUser")]
        public async Task UpdateUser(UserDTO userDTO)
        {
            try
            {
                await _userService.UpdateUser(userDTO);
                await Clients
                        .All
                        .SendCoreAsync("UserProfileChanged", new object[] { userDTO });
            }
            catch (Exception ex)
            {
                await Clients.Caller.SendCoreAsync("ServerError", new object[] { ex.Message });
            }
        }
        
        /// <summary>
        /// Используется для получения пользователя по ID
        /// </summary>
        /// <param name="id">ID пользователя</param>
        /// <returns>Пользователь по ID</returns>
        [HubMethodName("GetUserById")]
        public async Task<ResponseDTO> GetUserById(int id)
        {
            try
            {
                UserDTO userDTO = await _userService.GetUserById(id);
                return new ResponseDTO
                {
                    IsSuccess = true,
                    Result = userDTO
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        /// <summary>
        /// Используется для получения чата
        /// </summary>
        /// <param name="currentUserId">ID текущего пользователя</param>
        /// <param name="participateUserId">ID собеседника</param>
        /// <returns>Чат</returns>
        [HubMethodName("GetChatListItemByCurrentUserAndParticipateUserId")]
        public async Task<ResponseDTO> GetChatListItemByCurrentUserAndParticipateUserId(GetChatListItemByCurrentUserAndParticipateUserIdDTO getChatListDTO)
        {
            try
            {
                UserChatDTO? userChatDTO = await _userService.GetChatListItemByCurrentUserAndParticipateUserId(getChatListDTO.CurrentUserId, getChatListDTO.ParticipateUserId);
                return new ResponseDTO
                {
                    IsSuccess = true,
                    Result = userChatDTO
                };
            }
            catch (Exception ex)
            {
                return new ResponseDTO()
                {
                    IsSuccess = false,
                    ErrorMessage = ex.Message,
                    TypeOfException = ex.GetType() == typeof(ServerException) ? TypesOfExceptions.SERVER : TypesOfExceptions.UNEXPECTED
                };
            }
        }

        #endregion
    }
}
