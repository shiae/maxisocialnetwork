﻿namespace MaxiSocialNetworkChecker
{
    /// <summary>
    /// Базовый класс для реализации проверок
    /// </summary>
    public abstract class BaseChecker<P>
    {
        /// <summary>
        /// Используется для реализации проверки данных
        /// </summary>
        public abstract void Check(P parameter);
    }
}
