﻿using MaxiSocialNetworkChecker.Exceptions;
using System.Text.RegularExpressions;

namespace MaxiSocialNetworkChecker
{
    /// <summary>
    /// Наследование <see cref="BaseChecker"/>
    /// </summary>
    public class EmailChecker : BaseChecker<string>
    {
        public const string NOT_CORRECT_EMAIL_ERROR_MSG = "Была введена некорректная почта";

        /// <summary>
        /// Используется для проверки почты
        /// </summary>
        /// <param name="parameter">Почта</param>
        public override void Check(string email)
        {
            Regex regex = new Regex(@"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$");
            Match match = regex.Match(email);

            if (!match.Success)
            {
                throw new BaseCheckerException(NOT_CORRECT_EMAIL_ERROR_MSG);
            }
        }
    }
}
