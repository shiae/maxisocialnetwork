﻿using MaxiSocialNetworkChecker.Exceptions;

namespace MaxiSocialNetworkChecker
{
    public class PasswordChecker : BaseChecker<string?>
    {
        public const string MINIMUL_LENGTH_ERROR = "Длина пароля должна быть не менее 5 символов";
        public const string FORMAT_NOT_MATCH = "Пароль не соответствует требованиям: обязательно минимум 5 букв, из них 1 цифра, 1 большая буква, 1 маленькая буква и 1 символ";

        public override void Check(string? password)
        {
            if (password == null || password.Length < 5)
            {
                throw new BaseCheckerException(MINIMUL_LENGTH_ERROR);
            }

            if (!password.Any(char.IsDigit) || !password.Any(char.IsUpper) || !password.Any(char.IsLower) || password.All(char.IsLetterOrDigit))
            {
                throw new BaseCheckerException(FORMAT_NOT_MATCH);
            }
        }
    }
}