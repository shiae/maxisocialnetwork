﻿using MaxiSocialNetworkChecker.Exceptions;
using System.Text.RegularExpressions;

namespace MaxiSocialNetworkChecker
{
    public class PhoneChecker : BaseChecker<string>
    {
        public const string NOT_CORRECT_PHONE_ERROR_MSG = "Был введен некорректный номер телефона";

        public override void Check(string phoneNumber)
        {
            string pattern = @"^(\+7|8)\d{10}$";
            Regex regex = new Regex(pattern);

            // Проверяем номер телефона
            if (!regex.IsMatch(phoneNumber))
            {
                throw new BaseCheckerException(NOT_CORRECT_PHONE_ERROR_MSG);
            }
        }
    }
}
