﻿using MaxiSocialNetworkChecker.Exceptions;
using System.Text.RegularExpressions;

namespace MaxiSocialNetworkChecker
{
    public class LoginChecker : BaseChecker<string?>
    {
        public const string LOGIN_CAN_NOT_BE_EMPTY = "Логин не может быть пустым!";
        public const string LOGIN_CAN_CONTAINS_LETTERS_AND_DIGITS = "Логин может содержать только английские буквы и цифры!";
        public const string LOGIN_MUST_CONTAINS_LETTERS_AND_DIGITS = "Логин должен содержать и цифры, и буквы!";

        public override void Check(string? parameter)
        {
            if (string.IsNullOrEmpty(parameter))
            {
                throw new BaseCheckerException(LOGIN_CAN_NOT_BE_EMPTY);
            }

            if (!Regex.IsMatch(parameter, "^[a-zA-Z0-9]+$"))
            {
                throw new BaseCheckerException("Логин может содержать только английские буквы и цифры!");
            }

            if (!parameter.Any(char.IsDigit) || !parameter.Any(char.IsLetter))
            {
                throw new BaseCheckerException(LOGIN_MUST_CONTAINS_LETTERS_AND_DIGITS);
            }
        }
    }
}
