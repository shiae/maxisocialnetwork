﻿namespace MaxiSocialNetworkChecker.Exceptions
{
    public class BaseCheckerException : Exception
    {
        public BaseCheckerException(string message) : base(message)
        {

        }
    }
}
